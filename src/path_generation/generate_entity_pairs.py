#!/usr/bin/python
import random
import os,sys
import networkx as nx
import configparser
from load_data  import nell,freebase,wikispeedia,yago
import random


app_config = configparser.ConfigParser()
if(len(sys.argv) != 2):
  print("Provide dataset name (nell/yago/wikispeedia/freebase)")
  sys.exit(1)
dataname = sys.argv[1]
config_file= "../../config/" +  dataname  + "/" + dataname + ".cfg"
print("Reading config from..", config_file)
app_config.read(config_file)

def read_entity_list(person_file, org_file, loc_file, thing_file):
  persons = []
  orgs = []
  locs = []
  things = []
  f1 = open(person_file, "r") 
  f2 = open(org_file, "r") 
  f3 = open(loc_file, "r") 
  f4 = open(thing_file, "r") 
  for fileptr, entity_list in zip([f1,f2,f3,f4], [persons, orgs, locs,things]):
    for line in fileptr:
      if(len(line) > 0 and not line.startswith("#")):
         entity_list.append(line.strip())
  return(persons,orgs, locs,things)

def create_entity_pairs_from_catg(persons,orgs, locs,things, outfile):
    fout = open(outfile, "w")
    pp = []
    po = []
    pl = []
    pt = []
    oo = []
    ol = []
    ot = []
    ll = []
    lt = []
    tt = []
    for mylist, mypairs in zip([persons,orgs, locs,things], [pp, oo, ll, tt]):
        for source in mylist:
            target = random.choice(mylist)
            mypairs.append((source,target))
    for p in persons:
        for o in orgs:
            po.append((p,o))
        for l in locs:
            pl.append((p,l))
        for t in things:
            pt.append((p,t))
    for o in orgs:
        for l in locs:
            ol.append((o,l))
        for t in things:
            ot.append((o,t))
    for l in locs:
        for t in things:
            lt.append((l,t))

    for mylists, header in zip([pp, po, pl, pt, oo, ol, ot, ll, lt, tt],
                       ["#Person-Person", "#Person-Org", "#Person-Location",
                        "#Person-Thing", "#Org-Org", "#Org-Loc", "#Org-Thing",
                        "#Loc-Loc", "#Loc-Thing", "#Thing-THing"]):
        fout.write(header + "\n")
        for ep in mylists:
            fout.write(ep[0] +"\t" + ep[1] + "\n")
    fout.close()
    return

def get_graph(graphfile):
  G = nx.Graph()
  f = open(graphfile, "r")
  for line in f:
    if(len(line) > 0  and not line.startswith("#")):
       arr = line.strip().split("\t")
       if(len(arr) == 3):
          G.add_edge(arr[0], arr[2], label=arr[1])
       elif(len(arr) == 2):
          G.add_edge(arr[0], arr[1])
       else:
          print("find a line in expected format", line)
  print(G.number_of_nodes(), G.number_of_edges())
  f.close()
  return G

# given a set of selected nodes, get entity pairs , where
#  mindepth < shortest_path_dist < max_depth
def get_shortest_path_entitypairs(snodes, G, outfile, mindepth, maxdepth):
  fout = open(outfile , "w")
  fout.write("src\t target\t shortest_path_dist\n")
  entityPairs = []
  for srcnode in snodes:
    node_pathlen = nx.single_source_shortest_path_length(G, srcnode, cutoff=maxdepth)
    #valid_nodes = [x for x in node_pathlen if node_pathlen[x] >= mindepth]
    #print("valid nodes", valid_nodes)
    #selected_random = random.choice(valid_nodes)
    #entityPairs.append((srcnode, selected_random))
    for depth in range(mindepth, maxdepth):
      found = False
      for n in node_pathlen:
        if(node_pathlen[n] == depth and found == False):
          fout.write(srcnode + "\t" + n + "\t" + str(node_pathlen[n]) + "\n")
          found = True
    #   ep = (srcnode, n)
    #   entityPairs.append(ep)
  # need to do unique, but there seesm to be an error in unique, getting 
  # redundant 1 single entity pair
  #print("Done generating entity pairs..., reducing to unique",
  #      len(entityPairs))
  #entityPairs = unique(entityPairs)
  #for ep in entityPairs:
  #  fout2.write(srcnode + "\t" + n + "\t" + str(node_pathlen[n]) + "\n")
  print("Done.. writing entity pairs to file :", outfile)
  return 

# given a set of entity pairs , select only those wherei,
# shortest_pth_dist < max_depth
def select_entitypairs_with_max_shortest_path(entity_pairs,
                                              G, outfile, max_shortest_path):
  fout = open(outfile + "nodes.bfs.log", "w")
  for ep in entity_pairs:
    srcnode = ep[0]
    node_pathlen = nx.single_source_shortest_path_length(G, srcnode, cutoff=maxdepth)
    for n in node_pathlen:
      if(n == ep[1]):
        fout.write(srcnode + "\t" + n + "\t" + str(node_pathlen[n]) + "\n")
  return

# Tries to generate all paths for given entity pair, 
#very slow, not suitable for large graphs
def get_all_paths_entity_pairs(entityPairs, G, path_depth):
  for ep in entityPairs:
    print("Getting entity pair paths", ep)
    allpaths = nx.all_simple_paths(G, ep[0], ep[1], cutoff=path_depth)
    if(len(list(allpaths)) >= 5):
      fout = open(ep[0] + "_" + ep[1] + ".paths.txt", "w")
    else:
      fout = open(ep[0] + "_" + ep[1] + ".paths.lt5.txt", "w")
    for x in list(allpaths):
      fout.write(str(x) + "\n")
  fout.close()
    
def unique(entityPairs):
  mydict = {}
  for ep in entityPairs:
    epr = (ep[1], ep[0])
    if(ep not in mydict and epr not in mydict):
      mydict[ep] = 1
  return mydict.keys()



if __name__ == "__main__":
    dataset = dataset = app_config['files']['datatype']
    if(dataset == "freebase"):
        dataobj = freebase()
    elif(dataset == "yago"):
        dataobj = yago()
    elif(dataset == "wikispeedia"):
        dataobj =wikispeedia()
    elif(dataset == "nell"):
        dataobj = nell()
    else:
        print("Unknown data format")
        sys.exit(1)

    graph = dataobj.load_graph(app_config['files']['graph_path'])
    outdir = app_config['files']['datahome']
    
    # For freebase since most of the random entities were person
    # ensuring coverage across node types 
    #person_file = "selected_persons.txt"
    #org_file = "selected_orgs.txt"
    #loc_file = "selected_locs.txt"
    #thing_file = "selected_things.txt"
    #persons, orgs, locs,things = read_entity_list(person_file,
                                               #org_file,loc_file,thing_file)
    #all_eps = create_entity_pairs_from_catg(persons,orgs, locs,things, outfile)
    #entityPairs = select_entitypairs_with_max_shortest_path(all_eps, graph, "entitypairs.shortestpath.txt", max_shortest_path=5)

    ## For nell, generating random pairs
    selected_nodes = list(graph.nodes())
    mindepth = 2
    maxdepth = 4
    outfile = outdir + "/entitypairs.shortest_dist_" + str(mindepth) + "_" + str(maxdepth) + ".txt"
    entityPairs = get_shortest_path_entitypairs(selected_nodes, graph,outfile, mindepth, maxdepth)



    





            


