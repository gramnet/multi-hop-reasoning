import io,sys
from scipy.spatial import distance
import numpy as np

def get_coherence(path, node_embeddings):
    path_embeddings = [node_embeddings[n] for n in path]
    total_dist = 0.0
    for i in range(0, len(path)-1):
        total_dist += distance.cosine(np.asarray(path_embeddings[i],dtype='float32'),
                                      np.asarray(path_embeddings[i+1],dtype='float32'))
    return total_dist/np.float(len(path)-1)



# Implements metro maps coherence
# Identify top k concepts in the path
# Look at divergence of the path in those concepts only
# consider the influence of each concept
# returns the minimal coherent link for the maximal coherent topic
def get_coherence2(path, node_embeddings, num_active_concepts):
  path_len = len(path)
  path_embeddings = [node_embeddings[n] for n in path]
  average_path_embedding = np.average(np.asarray(path_embeddings, dtype='float32'), axis=0)
  # get active concept
  sorted_concept_ids = np.argsort(average_path_embedding)
  selected_concept_ids = sorted_concept_ids[-num_active_concepts:]
  #print("average embedding and active concepts", average_path_embedding, selected_concept_ids)
  max_coherence_concept = 0.0
  # get_path as a sequence of node_embeddings for these active concepts
  path_embeddings_selected_concepts = []
  for i in range(0, path_len):
    node_embedding = path_embeddings[i]
    node_embedding_selected_concept = [node_embedding[concept_id] for concept_id in selected_concept_ids]
    path_embeddings_selected_concepts.append(node_embedding_selected_concept)
  average_path_embeddings_selected_concepts = [average_path_embedding[i] for i in selected_concept_ids]

  #Calculate pair wise divergence of sequence per concept
  min_distance = []
  for concept_id in range(0, num_active_concepts):
    max_dist = -1.0
    for pos in range(0, path_len-1):
      d = abs(float(path_embeddings_selected_concepts[pos][concept_id]) - float(path_embeddings_selected_concepts[pos+1][concept_id]))
      dist = d*d
      if(dist > max_dist):
         max_dist = dist
    min_distance.append(max_dist)
    #min_distance.append(max_dist*average_path_embeddings_selected_concepts[concept_id])
  return(min(min_distance))
