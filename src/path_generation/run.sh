#!/bin/bash

##SBATCH -A project_name
#SBATCH -t 23:59:00
#SBATCH -N 1
#SBATCH -p gpu

echo $1
module load cuda python/anaconda2
python generate_paths_entity_pairs.py $1

