#!usr/bin/python
import io, sys
import numpy as np
from coherence_measures import get_coherence, get_coherence2
from load_data import yago, wikispeedia, freebase, nell
import configparser


app_config = configparser.ConfigParser()
if(len(sys.argv) < 2 ):
  print("Provide dataset name (nell/yago/wikispeedia/freebase)")
  sys.exit(1)
dataname = sys.argv[1]
config_file= "../../config/" +  dataname  + "/" + dataname + ".cfg"
print("Reading config from..", config_file)
app_config.read(config_file)


# Given a question and a set of answers, pick 5 answers such that ythey fall into diffrent kin dof answer categories. Answer categories are detemined based on coherence, specificity and length

# generate question-> {answers} dictionary
def get_question_answer_dict(filename):
  f = open(filename, "r")
  question_answer_dict = {}
  for line in f:
    arr = line.strip().split(",")
    if(len(arr) != 3):
      continue
    question = (arr[0], arr[1])
    answer_path = arr[2]
    if(question not in question_answer_dict):
      question_answer_dict[question] = [answer_path]
    else:
      answers = question_answer_dict[question]
      answers.append(answer_path)
      question_answer_dict[question] = answers
  return question_answer_dict

# Reads file containing path to path output file per entity pair
# num_paths filename(ep1__ep2.txt)
# e.g.
# 8   alfred_hitchcock__isaac_asimov.all_paths.txt.selected.txt
def get_question_answer_dict_ep_file_format(questions_file, indir):
    f = open(questions_file, "r")
    question_answer_dict = {}
    for line in f:
        question_answer_pathfile = line.strip()
        f2 = open(indir + "/" + question_answer_pathfile, "r")
        for line in f2:
            path = line.strip()[1:-1].split(", ")
            nodes = [x[1:-1] for x in path]
            question = (nodes[0], nodes[-1])
            answer_path = line.strip()[1:-1] # removing leading and trailing brackets
            if(question not in question_answer_dict):
                question_answer_dict[question] = [answer_path]
            else:
                answers = question_answer_dict[question]
                answers.append(answer_path)
                question_answer_dict[question] = answers
    return question_answer_dict



def write_all_paths_to_category_file(question, question_id, p1, f1,
                                    p2,f2,p3,f3,p4,f4,p5,f5):
  for paths_with_features,fileout in zip([p1,p2,p3,p4,p5], [f1,f2,f3,f4,f5]):
    for pf in paths_with_features:
        path = pf[0]
        features = pf[1:]
        str_path = ";".join(str(node) for node in path)
        #print(str_path)
        fileout.write(str(question) + "\t" + str(question_id) + "\t" + str_path + "\t" + str(features) + "\n")
  return
        
# FGiven question-> {answers} dictionary,
# sample answers based on answer properties
def sample_answers(question_answer_dict, node_embeddings, node_degrees, output_path, dataobj):
  short_path_length = 4
  fout1 = open(output_path, "w")
  fout2 = open(output_path + ".features.txt" , "w")
  fout3 = open(output_path + ".amazon.csv" , "w")
  question_id = 0
  f_all_short_paths = open(output_path + ".all_short_paths", "w")
  f_all_hh_paths = open(output_path + ".all_high_coh_high_deg_paths", "w")
  f_all_hl_paths = open(output_path + ".all_high_coh_low_deg_paths", "w")
  f_all_lh_paths = open(output_path + ".all_low_coh_high_deg_paths", "w")
  f_all_ll_paths = open(output_path + ".all_low_coh_low_deg_paths", "w")
  for question, answer_paths in question_answer_dict.items():
    # For this question, get average path features
    paths_with_features= []
    total_div_paths = 0.0
    total_metro_maps_div_paths  = 0.0
    total_degree_paths = 0
    for path in answer_paths:
      #print("Question, answer = ", question, path)
      p = dataobj.get_nodes_from_answer_user_study(path)
      divergence_path = get_coherence(p, node_embeddings)
      metro_maps_divergence_path = get_coherence2(p, node_embeddings, 3)
      degree_path = max([node_degrees[n] for n in p])
      path_len = len(p)
      paths_with_features.append([p, path_len, divergence_path, metro_maps_divergence_path, degree_path])
      total_div_paths += divergence_path
      total_metro_maps_div_paths += metro_maps_divergence_path
      total_degree_paths += degree_path 
    average_div_paths = total_div_paths/len(answer_paths)
    average_metro_maps_div_paths = total_metro_maps_div_paths/len(answer_paths)
    average_degree_paths = total_degree_paths/len(answer_paths)
   
    # Divide paths by grouping them in different feature categories
    # Note the paths are grouped based on the question alone 
    short_answers = []
    long_answers_hh = []
    long_answers_hl = []
    long_answers_lh = []
    long_answers_ll = []  

    for pf in paths_with_features: 
      path_len = pf[1]
      divergence_path = pf[2]
      metro_maps_divergence_path = pf[3]
      average_degree_path = pf[4]
      
      # removes direct paths and keeps only short paths
      if(path_len > 2 and path_len <= short_path_length):
        short_answers.append(pf)
      elif(path_len > short_path_length and metro_maps_divergence_path <= average_metro_maps_div_paths and average_degree_path <= average_degree_paths):
        long_answers_hl.append(pf)
      elif(path_len > short_path_length and metro_maps_divergence_path > average_metro_maps_div_paths and average_degree_path <= average_degree_paths):
        long_answers_ll.append(pf)
      elif(path_len > short_path_length and metro_maps_divergence_path <= average_metro_maps_div_paths and average_degree_path > average_degree_paths):
        long_answers_hh.append(pf)
      elif(path_len > short_path_length and metro_maps_divergence_path > average_metro_maps_div_paths and average_degree_path > average_degree_paths):
        long_answers_lh.append(pf)
  
    
    write_all_paths_to_category_file(question, question_id, 
                                     short_answers, f_all_short_paths,
                                     long_answers_hh, f_all_hh_paths,
                                     long_answers_hl, f_all_hl_paths,
                                     long_answers_lh, f_all_lh_paths,
                                     long_answers_ll, f_all_ll_paths)
    
    #question_final_answers = []
    #print(str(question) , len(short_answers), len(long_answers_hh), len(long_answers_hl) , len(long_answers_lh) , len(long_answers_ll))
    #while(len(question_final_answers) < 10):
    #  if(len(short_answers) >= 1):
    #    selected = np.random.choice(len(short_answers), 1)
    #    question_final_answers.append((short_answers[selected[0]], "short"))
    #  if(len(long_answers_hh) >= 1):
    #    selected = np.random.choice(len(long_answers_hh), 1)
    #    question_final_answers.append((long_answers_hh[selected[0]], "hh"))
    #  if(len(long_answers_hl) >= 1):
    #    selected = np.random.choice(len(long_answers_hl), 1)
    #    question_final_answers.append((long_answers_hl[selected[0]], "hl"))
    #  if(len(long_answers_lh) >= 1):
    #    selected = np.random.choice(len(long_answers_lh), 1)
    #    question_final_answers.append((long_answers_lh[selected[0]], "lh"))
    #  if(len(long_answers_ll) >= 1):
    #    selected = np.random.choice(len(long_answers_ll), 1)
    #    question_final_answers.append((long_answers_ll[selected[0]], "ll"))

    #paths_str = ""
    #already_used = []
    #for pf in question_final_answers:
    #  path = pf[0][0]
    #  if(path in already_used):
    #    continue
    #  already_used.append(path)
    #  str_path = ";".join(str(node) for node in path)
    #  str_features = "\t".join(str(x) for x in pf[0][1:])
    #  path_category = pf[1]
    #  fout1.write(str(question) + "\t" + str(question_id) + "\t" + str_path + "\n")
    #  fout2.write(str(question) + "\t" + str(question_id) + "\t" + str_features + "\t" + path_category + "\n")
    #  paths_str += str_path + ","
    #fout3.write(str(question[0]) + ", " + str(question[1]) + ", " + paths_str + "\n")
    #print("Number of unique paths for question =", str(question), len(already_used))
    question_id += 1
  
  #fout1.close()
  #fout2.close()
  #fout3.close()
  f_all_short_paths.close()
  f_all_hh_paths.close()
  f_all_hl_paths.close()
  f_all_lh_paths.close()
  f_all_ll_paths.close()
  return
      
if __name__ == "__main__":
  if(len(sys.argv) != 4):
    print("Usage <inputpath(containing_list_of_answer_files)> <outpath_file_prefix>")
  qa_file = sys.argv[2]
  output_path = sys.argv[3]
  
  indir = app_config['files']['datahome'] + "/training_data_gen_minerva/paths"
  dataset = app_config['files']['datatype']
  if(dataset == "yago"):
    dataobj = yago()
  elif(dataset == "wikispeedia"):
    dataobj =wikispeedia()
  elif(dataset == "freebase"):
    dataobj =freebase()
  elif(dataset == "nell"):
    dataobj = nell()
  else:
    print("Unknown data format")
    sys.exit(1)
  node_degrees = dataobj.load_degree(app_config['files']['node_degree_path'])
  node_embeddings = dataobj.load_node_label_embedding_map(app_config['files']['node_embedding_path'])
  #question_answer_dict = get_question_answer_dict_qa_format(qa_file)
  question_answer_dict = get_question_answer_dict_ep_file_format(qa_file, indir)
  print("size of qa dctionary, node degrees, node embdeddigs ", len(question_answer_dict) , len(node_degrees) , len(node_embeddings))
  sample_answers(question_answer_dict, node_embeddings, node_degrees, output_path, dataobj)
  
    

    
   
   
    
      
      
  
