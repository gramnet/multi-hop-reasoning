#!/usr/bin/python
import numpy as np
import io, sys
from load_data import yago,wikispeedia,freebase,nell,metaqa
from scipy.spatial import distance
import configparser
import networkx as nx
import random
# generate

app_config = configparser.ConfigParser()
if(len(sys.argv) != 3):
    print("Provide dataset name (nell/yago/wikispeedia/freebase)" + 
          " , path_to_entity_pair_file")
    sys.exit(1)
dataname = sys.argv[1]
config_file= "../../config/" +  dataname  + "/" + dataname + ".cfg"
print("Reading config from..", config_file) 
app_config.read(config_file)

def generate_paths(questionpairs, graph, seq_len, topk_nbrs, use_nbrs, node_embeddings, node_degrees, outdir):
  allPaths = []
  for qp in questionpairs:  
    paths = generate_path(qp, graph, seq_len, topk_nbrs, node_embeddings, node_degrees, [qp[0]], use_nbrs)
    fout = open(outdir + "/" + qp[0] + "__" + qp[1] + ".all_paths.txt", "w")
    for p in paths:
      p.insert(0, qp[0])
      fout.write(str(p) + "\n")
    #sampled_paths = sampled_paths_user_study(paths)
    fout.close()


# Generate predicted paths for given pair of entities using model and graph properties
def generate_path(qp, graph, max_seq_len, topk, node_embeddings, node_degrees, currpath, use_nbrs):
  curr_length = len(currpath)
  source = qp[0]
  target = qp[1]
  if(curr_length > max_seq_len or source == target):
    return []

  allPaths = []
   
  # For simple walk, without model
  if (use_nbrs == "coherence"):
    target_emb = node_embeddings[target]
    topk_nbrs = get_topk_neighbors(source, graph, node_embeddings, node_degrees, target_emb, topk)
  elif (use_nbrs == "random"):
    neighbors = nx.all_neighbors(graph, source)
    neighbor_list = [n for n in neighbors]
    topk_nbrs = np.random.choice(neighbor_list, size=topk)
  elif(use_nbrs == "pattern"):
    print("Define function  get_topk_neighbors_pattern()")
    sys.exit(1)
    #topk_nbrs = get_topk_neighbors_pattern(source, graph, topk)
  else:
    print("Define function  get_topk_neighbors_pattern()")
    sys.exit(1)
 
  # Check if  target is directly reachable, append as valid path
  source_all_neighbors = nx.all_neighbors(graph, source)
  source_neighbor_list = [n for n in source_all_neighbors] 
  if(target in source_neighbor_list): 
    path = [target]
    allPaths.append(path)

  for n in topk_nbrs:
    if(n != target and n not in currpath):
      nbr_qp = [n, target]
      newpath =  list(currpath)
      newpath.append(n)
      nbr_paths = generate_path(nbr_qp, graph, max_seq_len, topk, node_embeddings, node_degrees, newpath, use_nbrs)
      newpath = newpath[0:-1]
      #print("Found paths between", nbr_qp, nbr_paths)
      for p in nbr_paths:
        p.insert(0, n)
      allPaths.extend(nbr_paths)
  return allPaths

def get_topk_neighbors(input_node, graph, node_embeddings, node_degrees, pred_label_emb, k):
  neighbors = nx.all_neighbors(graph, input_node)
  neighbor_list = [n for n in neighbors]
  neighbor_distances = [distance.cosine(np.asarray(node_embeddings[n],
      dtype='float32'), pred_label_emb) for n in neighbor_list]
  neighbors_by_dist = [neighbor_list[i] for i in np.argsort(neighbor_distances)]
  if len(neighbors_by_dist) < k:
    return neighbors_by_dist
  else:
    return neighbors_by_dist[0:k] 

def get_entity_pairs(entityPairFile):
  entityPairs = []
  f = open(entityPairFile)
  for line in f:
    if(line.startswith("#")):
      continue
    ep = line.strip().split("\t")
    if(len(ep) >= 2):
      entityPairs.append((ep[0], ep[1]))
    else:
      print("found a line in expected format", line)
  return entityPairs


if __name__ == "__main__":
  dataset = dataset = app_config['files']['datatype']
  if(dataset == "freebase"):
    dataobj = freebase()
  elif(dataset == "yago"):
    dataobj = yago()
  elif(dataset == "wikispeedia"):
    dataobj =wikispeedia()
  elif(dataset == "nell"):
    dataobj = nell()
  elif(dataset == "metaqa"):
    dataobj = metaqa()
  else:
    print("Unknown data format")
    sys.exit(1)
    
  entity_pairs = get_entity_pairs(sys.argv[2])
  #entity_pairs = app_config['files']['train_pairs']
  #entity_pairs = [("concept_personcanada_jim_rutenberg",
  #                 "concept_musicartist_times")]
  print("Number of entity pairs =", len(entity_pairs))
  node_degrees = dataobj.load_degree(app_config['files']['node_degree_path'])
  node_embeddings = dataobj.load_node_label_embedding_map(app_config['files']['node_embedding_path'])
  graph = dataobj.load_graph(app_config['files']['graph_path'])
  seq_len = 5
  topk_nbrs = 5
  use_nbrs = "random"
  outdir = app_config['files']['datahome'] + "/entity_pair_paths/"
  #os.mkdir(outdir)
  print("Writing random walks path to ", outdir)
  generate_paths(entity_pairs, graph, seq_len, topk_nbrs, use_nbrs,
                 node_embeddings, node_degrees, outdir)



