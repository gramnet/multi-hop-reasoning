#!/bin/bash
dataname=nell

#python code generates lot of entity pairs, randomly downselect 
#python generate_entity_pairs.py $dataname
#shuf -n $target_train_pairs entitypairs.shortest_dist_2_4 > selected_entity_pairs_nell.txt
#cat entitypairs.shortest_dist_2_4.txt entitypairs.shortest_dist_2_3.txt > selected_entity_pairs_nell.txt
#python generate_paths_entity_pairs.py $dataname selected_entity_pairs_nell.txt
#ls /home/d3x771/datasets/nell/paths/ > /home/d3x771/datasets/nell/all_training_pair_paths.list.txt
basedir=/lustre/d3x771/question_answering_datasets/minerva-datasets/nell/training_data_gen/
for infile in $basedir/entity_pair_split/xe*
do
		filename=$(basename $infile)
		outdir=$basedir/entity_pair_paths/
		outfile=$outdir$filename
		echo $infile
		echo $outfile
		python sample_paths_by_categories.py  $dataname $infile $outfile
done

#cat all_desired_path_results, sort , get unique, shuffle and get only paths for training model
#cat training_paths.all_high_coh_high_deg training_paths.all_short_paths | sort | uniq | shuf | awk - F "\t" '{print $3}' > final_training_paths
