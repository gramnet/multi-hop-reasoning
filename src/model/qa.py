import tensorflow as tf
import numpy as np
import networkx as nx
from scipy.spatial import distance
import keras
from keras.callbacks import ModelCheckpoint
import sys, configparser
import keras.backend as K
import time
from load_data import yago,wikispeedia,freebase,nell,metaqa
#from mpi4py import MPI
import os.path
from keras.models import load_model
from keras import regularizers

use_mpi = False
app_config = configparser.ConfigParser()
if len(sys.argv) != 2:
  print("Provide path to config file")
  sys.exit(1)
app_config.read(sys.argv[1])
#if (app_config['training']['use_mpi'] == 'yes'):
  #use_mpi = True
  #comm = MPI.COMM_WORLD
  #rank = comm.Get_rank()
  #size = comm.Get_size()
rank = 0
size = 1

def load_node_id_map(node_id_map_path):
  f = open(node_id_map_path)
  node_id_map = dict()
  for line in f:
    tokens = line.strip().split('\t')
    node_label = tokens[0]
    node_id = tokens[1]
    node_id_map[node_label] = node_id
  f.close()
  return node_id_map

def get_node_embeddings(node_id_map_path, node_embedding_path):
  node_id_map = load_node_id_map(node_id_map_path)
  node_embeddings = load_node_label_embeddings_map(node_embedding_path)
  return (node_id_map, node_embeddings) 


def transform_labels_to_embeddings(nodelist, node_label_embedding_map):
  print('Converting labels to vector representation ...')
  res = [np.array(node_label_embedding_map[n], dtype='float32') for n in nodelist]
  return res

def get_average_embeddings(nodes, node_label_embedding_map):
  #print('Converting neighbours to mean vector representation ...')
  seq_embeddings = []
  for n in nodes:
    nodes_with_embeddings = np.array(node_label_embedding_map[n], dtype='float32')
    seq_embeddings.append(nodes_with_embeddings) 
  return np.average(seq_embeddings, axis=0)

def transform_paths_to_embeddings(paths, node_label_embedding_map, seq_len):
  print('Converting paths to vector representation ...')
  vector_space_paths = []
  for p in paths:
    if len(p) >= seq_len:
      path_with_embeddings = [np.array(node_label_embedding_map[n], dtype='float32') for n in p[0:seq_len]]
      vector_space_paths.append(path_with_embeddings)
  return vector_space_paths

def get_lstm_training_data(graph_paths_with_embeddings, batch_sz):
  print('Converting paths to training data and labels ...')
  num_training_seqs = batch_sz*int(len(graph_paths_with_embeddings)/batch_sz)
  print('Transforming ' + str(num_training_seqs) + ' from ' + str(len(graph_paths_with_embeddings)))
  training_seqs = []
  labels = []
  for i, p in enumerate(graph_paths_with_embeddings):
    if i < num_training_seqs:
      training_seqs.append(p[0:-1]) 
      labels.append(p[-1])
  num_sequences = len(graph_paths_with_embeddings)
  train_data = np.asarray(training_seqs, dtype='float32') 
  print(np.shape(train_data))
  train_labels = np.asarray(labels, dtype='float32')
  print(np.shape(train_labels))
  return train_data, train_labels


def get_topk_cands_sorted_by_vector_dist(graph, target_node, nbr_list, node_embeddings, topk):
  n_emb = np.asarray(node_embeddings[target_node], dtype='float32')
  #print(len(nbr_list))
  if(len(nbr_list) > 1000):
    nbr_list = np.random.choice(nbr_list, 1000)
  nbr_distances = [distance.euclidean(np.asarray(node_embeddings[n], dtype='float32'), n_emb) for n in nbr_list]
  nbrs_by_dist = [nbr_list[i] for i in np.argpartition(nbr_distances, topk)]
  return nbrs_by_dist[0:topk]
  

  
def get_topk_cands_sorted_by_vector_dist_prq(graph, target_node, nbr_list, node_embeddings, topk):
  n_emb = np.asarray(node_embeddings[target_node], dtype='float32')
  nbr_distances = [distance.euclidean(np.asarray(node_embeddings[n], dtype='float32'), n_emb) for n in nbr_list]
  temp =np.argpartition(nbr_distances, topk)
  nbrs_by_dist = [nbr_list[i] for i in temp[0:topk]]
  return nbrs_by_dist

def get_topk_cands_sorted_by_vector_dist_full_sort(graph, target_node, nbr_list, node_embeddings):
  n_emb = np.asarray(node_embeddings[target_node], dtype='float32')
  nbr_distances = [distance.euclidean(np.asarray(node_embeddings[n], dtype='float32'), n_emb) for n in nbr_list]
  nbrs_by_dist = [nbr_list[i] for i in np.argsort(nbr_distances)]
  return nbrs_by_dist[0:topk]
  
  

def get_neighbors_sorted_by_vector_dist(graph, node, node_embeddings):
  nbrs = nx.all_neighbors(graph, node)
  nbr_list = [n for n in nbrs]
  n_emb = np.asarray(node_embeddings[node], dtype='float32')
  #print('Frobenius norm = ' + str(np.linalg.norm(n_emb)))
  nbr_distances = [distance.euclidean(np.asarray(node_embeddings[n], dtype='float32'), n_emb) for n in nbr_list]
  nbrs_by_dist = [nbr_list[i] for i in np.argsort(nbr_distances)]
  print(zip(nbrs_by_dist, np.sort(nbr_distances)))
  return nbrs_by_dist

def build_seq_input(node_seq, max_len, node_embeddings):
  embed_sz = int(app_config['training']['embed_sz'])
  # batch_sz, seq_len-1, embed_sz
  #node_seq_with_embeddings = np.zeros((embed_sz, len(node_seq)))
  #node_seq_with_embeddings = np.zeros((1, len(node_seq), embed_sz))
  node_seq_with_embeddings = np.zeros((max_len, embed_sz))
  for i, n in enumerate(node_seq):
    #node_seq_with_embeddings[0,i,:] = np.asarray(node_embeddings[n]) 
    node_seq_with_embeddings[i,:] = np.asarray(node_embeddings[n]) 
  return node_seq_with_embeddings


# Q. what graph theoretic properties to guide the search?
# Q. how to estimate A* cost function?
# Q. implement backtracking. Try multiple templates and backtrack when an answer
#    becomes weak. Is it possible that coherence can drop and come back?
# Q. how to incorporate the concept of search acceleration?
# Q. how to incorporate explore/exploit? - adversarial search?
# Q. how to search from both ends?

# Q. implement convolution layers to support pattern based discovery
#    how are patterns useful in our context?

# Q. implement attention mechanism to guide the prediction
#    Use the neighbors to decide what template to pick
#    concatenate structural measures with embeddings

# What can we say from embeddings?  Where we still need to look up the graph?

# Address common nodes
# Address verification in the graph.  look ahead and verify

# Expand to general subgraph isomorphism

def topk_candidates(paths_with_embeddings, beam_sz):
  scores = [get_coherence(p) for p in paths_with_embeddings]
  ranked_paths = [paths_with_embeddings[i]  for i in argsort(scores)] 
  return ranked_paths[:beam_sz]

def beam_search(model, graph, s, t, max_len, beam_sz):
  candidates = []
  frontier = []
  candidates.append(s)
  final_answers = []

  for i in range(max_len):
    for candidate_seq in candidates:
      next_candidates = knn_search(candidate_seq)
      if t in next_candidates:
        answer = candidate_seq
        final_answers.append(answer.append(t))
      frontier.append(next_candidates) 
    candidates = topk_candidates(frontier, beam_sz)
    frontier = [] 

  print('Number of answer = ' + str(len(final_answers)))
  for answer in final_answers:
    print(answer)

def get_memory_model_training_data(graph_paths, batch_sz, seq_len):
  print('Converting paths to training data and labels ...')
  training_seqs = []
  labels = []
  for i, p in enumerate(graph_paths):
    if len(p) >= seq_len:
      #for index in range(0, len(p) - seq_len + 1):
      #  path = p[index:seq_len+index]
        path = p[0:seq_len]
        #training_seqs.append([path[0], path[-1]]) 
        #labels.append(path[1:-1])
        for s_pos in range(0, seq_len-2):
          training_seqs.append([path[s_pos], path[-1]]) 
          labels.append(path[s_pos+1])
  num_training_seqs = batch_sz*int(len(training_seqs)/batch_sz)
  print('Transforming ' + str(num_training_seqs) + ' from ' + str(len(training_seqs)))
  training_seqs = training_seqs[0:num_training_seqs]
  labels = labels[0:num_training_seqs]
  return training_seqs, labels
  #train_data = np.asarray(training_seqs) 
  #print(np.shape(train_data))
  #train_labels = np.asarray(labels)
  #print(np.shape(train_labels))
  #return train_data, train_labels

def get_memory_model_training_data2(graph_paths, batch_sz, seq_len_not_used):
  print('Converting paths to training data and labels ...')
  training_seqs = []
  labels = []
  for i, path in enumerate(graph_paths):
    len_path = len(path)
    if len_path >= 3:
      # number of nodes bwteen source and destination
      for source_destination_distance in range(1, len_path-1):
        # maximum source range is : start of sequence , end_of_seq -2
        for source_index in range(0, len_path-2):
          destination_index = source_index + source_destination_distance + 1
          if(destination_index < len(path)):
            training_seqs.append([path[source_index], path[destination_index]]) 
            labels.append(path[source_index+1])
  num_training_seqs = batch_sz*int(len(training_seqs)/batch_sz)
  print('Transforming ' + str(num_training_seqs) + ' from ' + str(len(training_seqs)))
  training_seqs = training_seqs[0:num_training_seqs]
  labels = labels[0:num_training_seqs]
  return training_seqs, labels


def generate_question_emb(question_pairs, graph, node_to_embedding_map, topk_nbrs, node_degrees):
  questions = []
  for pair in question_pairs:
    source = pair[0]
    target = pair[1]
    if source not in node_to_embedding_map or target not in node_to_embedding_map:
        print("Could not find embedidngs for ", source, target,
              len(node_to_embedding_map))
        sys.exit(1)
    src_emb = np.array(node_to_embedding_map[source], dtype='float32')
    target_emb = np.array(node_to_embedding_map[target], dtype='float32')
    if(topk_nbrs > 0):
      src_nbrs = get_topk_nbrs(nx.all_neighbors(graph, source), topk_nbrs, graph, target, node_to_embedding_map, node_degrees)
      target_nbrs = get_topk_nbrs(nx.all_neighbors(graph, target), topk_nbrs, graph, source, node_to_embedding_map, node_degrees)
      #question = [source, target] + src_nbrs + target_nbrs
      src_nbrs_emb = get_average_embeddings(src_nbrs, node_to_embedding_map) 
      target_nbrs_emb = get_average_embeddings(target_nbrs, node_to_embedding_map)
      questions.append([src_emb, target_emb, src_nbrs_emb, target_nbrs_emb])
    else:
      questions.append([src_emb, target_emb])
      
  #return transform_paths_to_embeddings(questions, node_to_embedding_map, 2+2*topk_nbrs)
  return questions


## TODO Improve this neighbour selection
def get_topk_nbrs(nbrs, topk, graph, target_node, node_embeddings, node_degrees):
  nbr_list = [n for n in nbrs]
  if(len(nbr_list) == topk):
    return nbr_list
  elif(len(nbr_list) < topk):
    return nbr_list + list(np.random.choice(nbr_list, topk-len(nbr_list)))
  else:
    if(app_config['training']['use_nbrs'] == "degree"):
      # pick one with most degree
      nbr_degrees = [node_degrees[n] for n in nbr_list]
      nbrs_sorted_by_degree = [nbr_list[i] for i in np.argsort(nbr_degrees)]
      return nbrs_sorted_by_degree[-topk:]
    elif(app_config['training']['use_nbrs'] == "coherence"):
      return get_topk_cands_sorted_by_vector_dist(graph, target_node, nbr_list, node_embeddings, topk)
    else:
      return list(np.random.choice(nbr_list, topk))
      

def model_size(model): 
  return sum([np.prod(K.get_value(w).shape) for w in model.trainable_weights])
    
def build_memory_model(train_questions_list, train_answers_list, graph, embed_sz, batch_sz, nepochs, dataset, topk_nbrs, seq_len):
  print('Building Memory model ...')
  #hidden_size = 512
  hidden_size = 32
  num_layers = 2
  #num_layers = 1
  train_questions = np.asarray(train_questions_list, dtype='float32')
  train_answers = np.asarray(train_answers_list, dtype='float32')
  print(np.shape(train_questions))
  print(np.shape(train_answers))
  
  #Building question encoder
  model = keras.models.Sequential() 
  if(topk_nbrs > 0):
    num_input_parameters = 4
  else:
    num_input_parameters = 2
  model.add(keras.layers.Bidirectional(
      #keras.layers.LSTM(512, stateful=True, return_sequences=False),
      #batch_input_shape=(batch_sz, 4, embed_sz)))
      keras.layers.LSTM(hidden_size,  return_sequences=False),
      input_shape=(num_input_parameters, embed_sz)))
  #model.add(keras.layers.Dropout(0.2))
  
  model.add(keras.layers.Dense(64, input_dim=64,
                kernel_regularizer=regularizers.l2(0.01),
                activity_regularizer=regularizers.l1(0.01)))
  #model.add(keras.layers.Dropout(0.5))
  model.add(keras.layers.Dense(embed_sz, activation='tanh'))
  # Encode the question in fixed size vector
  #model.add(keras.layers.LSTM(hidden_size, input_shape=(2+2*topk_nbrs, embed_sz)))
  #model.add(keras.layers.LSTM(hidden_size, input_shape=(4, embed_sz)))
  
  #model.add(keras.layers.RepeatVector(seq_len-2))
  #for _ in range(num_layers):
  #  model.add(keras.layers.LSTM(hidden_size, return_sequences=True))
  #model.add(keras.layers.Dropout(0.2))
  #model.add(keras.layers.TimeDistributed(keras.layers.Dense(embed_sz)))
  #model.add(keras.layers.Activation('softmax'))

  #model.add(keras.layers.Bidirectional(
  #    keras.layers.LSTM(512, stateful=True, return_sequences=False)))
  #    input_shape=(2+2*topk_nbrs, embed_sz)))
      #batch_size=batch_sz, input_shape=(seq_len-1, embed_sz)))
      #batch_input_shape=(batch_sz, seq_len-1, embed_sz)))
  #model.add(keras.layers.LSTM(32, stateful=True))
  #model.add(keras.layers.Bidirectional(
      #keras.layers.LSTM(32, stateful=True)))
  # model.add(keras.layers.LSTM(32, stateful=True, return_sequences=False,
  #   batch_input_shape=(batch_sz, seq_len-1, embed_sz)))
  #model.add(keras.layers.Dropout(0.2))
  #model.add(keras.layers.Dense(embed_sz, activation='softmax'))
  #model.compile(optimizer='adam', loss='categorical_crossentropy')
  model.compile(optimizer='adam', loss='cosine_proximity',metrics=None)#metrics=['accuracy'])
  #model.compile(optimizer='sgd', loss='cosine_proximity', metrics=None)
  
  checkpt_filepath = str(dataset) + "_" + app_config['training']['use_nbrs'] + "_" + str(topk_nbrs) + '_{epoch:02d}-{loss:0.4f}.hdf5'
  checkpoint = keras.callbacks.ModelCheckpoint(checkpt_filepath, 
     monitor='val_loss', verbose=1, save_best_only=True)

  es = keras.callbacks.EarlyStopping(monitor='val_loss', mode='min', verbose=1, patience=5)
  model.fit(train_questions, train_answers, epochs=nepochs, batch_size=batch_sz, callbacks=[checkpoint, es], validation_split=0.2, verbose=2)
  #model.fit(train_questions, train_answers, validation_split=0.2, epochs=nepochs, batch_size=batch_sz, verbose=2)
  if(use_mpi == False or (use_mpi == True and rank == 0)):
    print("MODEL #PARAMS : ", model_size(model))
  return model

def driver_learn(app_config):
  batch_sz = app_config['training'].getint('batch_sz')
  seq_len = app_config['training'].getint('seq_len')
  embed_sz = app_config['training'].getint('embed_sz')
  dataset = app_config['files']['datatype']
  epochs = app_config['training'].getint('epochs')
  topk_nbrs = app_config['training'].getint('topk_nbrs')
  print("Running config, number of epochs = ", app_config['training']['use_nbrs'], topk_nbrs, epochs)
  if(dataset == "yago"):
      dataobj = yago()
  elif(dataset == "wikispeedia"):
       dataobj =wikispeedia()
  elif(dataset == "freebase"):
       dataobj = freebase()
  elif(dataset == "nell"):
       dataobj = nell()
  elif(dataset == "metaqa"):
      dataobj = metaqa()
  else:
      print("Unknown data format")
      sys.exit(1)
 
  training_paths_file = app_config['files']['training_path'] 
  model_path = app_config['files']['model_path']
  print(training_paths_file)
  graph = dataobj.load_graph(app_config['files']['graph_path'])
  paths = dataobj.load_paths(training_paths_file, use_mpi, size, rank)
  node_degrees = dataobj.load_degree(app_config['files']['node_degree_path'])
  node_embeddings = dataobj.load_node_label_embedding_map(app_config['files']['node_embedding_path'])
  print("Size of node to degree dictionary", len(node_degrees))
  train_data, train_labels = get_memory_model_training_data2(paths, batch_sz, seq_len)
  train_questions = generate_question_emb(train_data, graph, node_embeddings, topk_nbrs, node_degrees)
  train_answers = transform_labels_to_embeddings(train_labels, node_embeddings)
  print("Starting model learning ....")
  stime = time.time()
  if(os.path.isfile(model_path)):
    print("Found existing model, continuing training..")
    model = load_model(model_path)
    model.fit(np.asarray(train_questions, dtype='float32'), 
              np.asarray(train_answers, dtype='float32'),
              validation_split=0.2, epochs=epochs, batch_size=batch_sz, verbose=2)
    model.save(model_path + "_iter2.h5")
  else:
    model = build_memory_model(train_questions, train_answers, graph, embed_sz, batch_sz, epochs, dataset, topk_nbrs, seq_len)
    model.save(model_path)
  etime = time.time()
  print("Time to perform model learning (seconds)=", etime-stime)
  return

if __name__ == "__main__":
  print("Starting training_main")
  driver_learn(app_config)
