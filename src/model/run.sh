#!/bin/bash

#SBATCH -N 1
#SBATCH -t 47:59:00
#SBATCH -p dl
#SBATCH --gres=gpu:2

##screen
##salloc -N 1 -t 23:59:00 -p dl --gres=gpu:2
##squeue
##ssh $node
##bash
cd /people/d3x771/projects/xqa/xqa/src/model
#infile=$1
module load cuda/9.0.176
module load python/anaconda2

infile=../../config/metaqa/metaqa.coh_20.cfg
echo $infile
#python qa.py $infile
python test_model.py $infile

#for infile in ../../config/nell/nell.cfg ../../config/wikispeedia/wikispeedia.cfg 
#for infile in ../../config/wikispreedia/wikispeedia.cfg 
#do
#		echo $infile
#		#python qa.py $infile
#		python vector_walk.py $infile
#		#python test_model.py $infile
#done
