#!/usr/bin/python
import os,sys
import numpy as np
import re
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

#@Input1: minerva outfile in format:
    #question   path    score
#@Input2: Best output model from XQA (best : num successful explanations)
    #original_path  orig_path_len   orig_path_coh  orig_path_coh2   orig_path_max_degree       (followed by for first predicted path which is to be ignored as it will pick the direct edges) followed by next_predicted_path_properties in tab format

#@Output : generate dict(question) -> (minerva_path_with_len_coh_specificity,xqa_path_with_len_coh_specificity)
# Ignore all question for which both minerva or xqa do not have an output
#@Output plot coherence, length and specificity



#Given predicetd paths features of all models, creates a histogram
# use it : when not comparing to question features
def plot_path_features_hist(path_features_all_model, legends, title,
                            xlabel,ylabel, selected_models, bins=5):
    path_features_selected_model = []
    selected_legends = []
    colors = ['crimson', 'orchid', 'darkblue', 'burlywood', 'green',]
    selected_colors = []
    for i in selected_models:
        path_features_selected_model.append(path_features_all_model[i])
        selected_legends.append(legends[i])
    selected_colors = colors[0:len(selected_models)]
    data = np.vstack(path_features_selected_model).T
    counts, _ = np.histogram(data, bins)
    print(title, xlabel, ylabel, selected_legends, bins, str(counts))
    plt.hist(data, bins, histtype='bar', color=selected_colors)
    plt.legend(selected_legends)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.savefig(title)
    plt.show()

def plot_path_features_line(path_features_all_model, legends, title,
                            xlabel,ylabel, selected_models, bins=5):
    path_features_selected_model = []
    selected_legends = []
    for i in selected_models:
        path_features_selected_model.append(path_features_all_model[i])
        selected_legends.append(legends[i])
    data = np.vstack(path_features_selected_model).T
    result_counts, _ = np.histogram(data, bins)
    for i in range(len(selected_models)):
        plt.plot(bins, result_counts[0])
    plt.legend(selected_legends)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.savefig(title)
    plt.show()

#Given predicted paths features of all models, creates a histogram
# use it : when not comparing to question features
# NOte : Normalized counts enabled 
def plot_path_features_linegraph(path_features_all_model, legends):
    nsamples = len(path_features_all_model[0])
    for path_data_model in path_features_all_model:
        result_counts, bins = np.histogram(path_data_model, bins=5)
        seq = [float(x)/nsamples for x in result_counts]
        plt.plot(bins[1:], seq)
    plt.legend(legends)
    plt.xlabel("Similarity to User Path")
    plt.ylabel("Percentage of Test Paths")
    plt.savefig(title)
    plt.show()
    return

#Plot user and predicted paths as function of question features
def plot_question_vs_path_features(question_features, answer_features_all, legends, xlabel, ylabel, title):
    colors = ['r*', 'bs', 'y+', 'go']
    for i, color in zip(answer_features_all, colors):
        predicted_path_features = i
        plt.plot(question_features, predicted_path_features, color)
    plt.legend(legends)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.savefig(title)
    plt.show()
    return

# Reads a pattern similatity tsv file and plot a histogram
def parse_pattern_sim(pattern_results_file):
    pattern_sim_all_test = [[], [], [], [], [], [], [], [], [], []]
    f = open(pattern_results_file)
    header = f.readline()
    cc = 0
    for line in f:
        arr = re.split('\t', line)
        if(len(arr) == 10):
            cc += 1
        for sim_score, pattern_sim_model in zip(arr, pattern_sim_all_test):
            try:
                x = float(sim_score)
                pattern_sim_model.append(x)
            except:
                pattern_sim_model.append(0.0)
    print(len(pattern_sim_all_test), len(pattern_sim_all_test[0]))
    return pattern_sim_all_test

def plot_num_successful_predicted_paths(datatype):
    # Data output from count_successful_predcitions.sh 
    if(datatype == 'wikispeedia'):
        num_nbrs = [5,10,20]
        num_pred_path_n0 = 4182
        num_pred_path_n5_coh = 4483
        num_pred_path_n10_coh = 4486
        num_pred_path_n20_coh = 4475
        num_pred_path_n5_deg = 4503
        num_pred_path_n10_deg = 4457
        num_pred_path_n20_deg = 4422
        num_pred_path_n5_random = 4238
        num_pred_path_n10_random = 4191
        num_pred_path_n20_random = 4476
        num_pred_path_coh =[num_pred_path_n5_coh,
                            num_pred_path_n10_coh,num_pred_path_n20_coh]
        num_pred_path_random =[num_pred_path_n5_random,num_pred_path_n10_random,
                               num_pred_path_n20_random]
        num_pred_path_deg = [num_pred_path_n5_deg, num_pred_path_n10_deg,
                             num_pred_path_n20_deg]

    elif(datatype == 'nell'):
        num_nbrs = [2,5,10]
        num_pred_path_n0 = 1540 
        num_pred_path_coh = [1434, 1313, 1414]
        num_pred_path_random = [1529,1389,1508]
    else:
        print("data for successful predicted paths is unvailable for dataset:",
              datatype)
        return

    plt.plot(num_nbrs, [num_pred_path_n0, num_pred_path_n0, num_pred_path_n0])
    plt.plot(num_nbrs, num_pred_path_coh)
    plt.plot(num_nbrs, num_pred_path_random)
    #plt.plot(num_nbrs, num_pred_path_degree)
    #plt.legend(('num_pred_paths_no_memory', 'num_pred_paths_coh', 'num_pred_paths_degree' ,
    plt.legend(('num_pred_paths_no_memory', 'num_pred_paths_coh',
                'num_pred_paths_random'))
    plt.xlabel("#Nbr_memory")
    plt.ylabel("#Successful Explanations")
    plt.savefig(datatype + "_num_success_explanations.png")
    plt.show()


def parse_pattern_scores(filename):
    f = open(filename, "r")
    header = f.readline()
    # data format:
    # user_path_pattern_min_score   user_path_pattern_sum_score pred_path_pattern_min_score_N=0   pred_path_pattern_sum_score_N=0...
    # predicted path data is availble for 10 diffrent models 
    path_scores_all = [[], [], [], [], [], [], [], [], [], [], []]
    for line in f:
        arr = re.split('\t', line)
        if(len(arr) != 11):
            #print("Not in expected format, #columns not 22", line)
            continue
        else:
            #print("found expected format, #columns =11")
            scores = arr
            for p, pseq in zip(scores, path_scores_all):
                try:
                    x = float(p)
                    pseq.append(x) 
                except: 
                    pseq.append(0.0) 
    print(len(path_scores_all), len(path_scores_all[0]))
    return path_scores_all

def count(myseq, threshold):
    count = 0
    for x in myseq:
        if(x >= threshold):
            count += 1
    return count


def plot_minerva_comparisons_all(question_answers_dict):

    print("Plotting paths, where both minerva and xqa were successful")
    #pattern_score_results = datadir + "/path_pattern_scores.tsv"
    print(" Number of common answers by both models", len(question_answers_dict))
    #print("Minerva : Average answer length, Average answer divergence, average max degree")
    #print("XQA : Average answer length, Average answer divergence, average max degree")
    #print("Users, na, na, na", np.average(minerva_answer_len),
    #      np.average(minerva_answer_coherence), np.average(minerva_answer_maxdegree))
    question_div = []
    question_shortest_path_len = []
    minerva_path = []
    minerva_answer_len  = []
    minerva_answer_div =  []
    minerva_answer_max_degree = []
    xqa_path = []
    xqa_answer_len  = []
    xqa_answer_div =  []
    xqa_answer_max_degree = []
    for question, results in question_answers_dict.iteritems():
        #print(results[2][2])
        question_div.append(results[0][1])
        question_shortest_path_len.append(results[0][0])
        minerva_answer_len.append(results[1][1])
        minerva_answer_div.append(results[1][2])
        minerva_answer_max_degree.append(results[1][3])
        xqa_answer_len.append(results[2][1])
        xqa_answer_div.append(results[2][2])
        xqa_answer_max_degree.append(results[2][3])
    
    legends = ["Minerva", "xqa_model"]
    plot_question_vs_path_features(question_div, [minerva_answer_div,
                                                  xqa_answer_div],
                                   legends,
                                   xlabel="Question Divergence",
                                   ylabel = "Path Divergence", 
                                   title = "Path Divergence vs Question divergence")
    plot_question_vs_path_features(question_shortest_path_len, [minerva_answer_div,
                                                  xqa_answer_div],
                                   legends,
                                   xlabel="Question Divergence",
                                   ylabel = "Path Divergence", 
                                   title = "Path Divergence vs Question divergence")
    

def  read_minerva_output(minerva_results):
    mydict = {}
    print("Reading data from", minerva_results)
    with open(minerva_results, "r") as f:
        for line in f:
            arr = line.strip().split("\t")
            if(len(arr)  < 5):
                print("Found an invalid line format", line)
            else:
                path = arr[0]
                pathlen= int(arr[1])
                path_coherence = float(arr[3])
                path_max_degree = int(arr[4])
                nodes = path.split(";")
                if(len(nodes) < 2):
                    print("Found an invalid path", path, line)
                    sys.exit(1)
                mydict[(nodes[0], nodes[-1])] = (nodes, pathlen, path_coherence,
                                                path_max_degree)
    #print(mydict[("concept_sportsteam_texas_tyler_patriots","concept_sportsleague_ncaa")])
    return mydict

def read_xqa_output(xqa_results, start_column_index):
    mydict = {}
    print("Reading data from", xqa_results)
    with open(xqa_results, "r") as f:
        for line in f:
            all_paths_with_features = line.strip().split("\t")
            if(len(all_paths_with_features) > start_column_index):
                arr = all_paths_with_features[start_column_index: start_column_index+5]
                valid_path = arr[0][1:-1].split(",")
                nodes = [ x.strip()[1:-1] for x in valid_path]
                pathlen= int(arr[1])
                path_coherence = float(arr[3])
                path_max_degree = int(arr[4])
                mydict[(nodes[0], nodes[-1])] = (nodes, pathlen, path_coherence,
                                            path_max_degree)
    #print(mydict[("concept_ceo_roger_ailes", "concept_company_fox")])
    return mydict

# creates a merged dictionary containing only keys 
# which are present in both input dictionaries
# resulting dictioary
# common_key -> (dict1.value, dict2.value)
def merge_dict(question_dict, dict1, dict2):
    merged_dict = {}
    for question, dict1_answer in dict1.iteritems():
        # other dictionaries contain data aboiut this question as well
        if(question in dict2 and question in question_dict):
            dict2_answer = dict2[question]
            question_features = question_dict[question]
            merged_dict[question] = (question_features, dict1_answer,
                                     dict2_answer)
    return merged_dict

def get_question_dict(question_propeties_file):
    mydict = {}
    with open(question_propeties_file, "r") as f:
        for line in f:
            if(not line.startswith("#")):
                arr = line.strip().split(",")
                if(len(arr) < 6):
                    print("iNvalid line, ignoring", line)
                    continue
                source = arr[1].strip()
                target = arr[2].strip()
                emb_dist = float( arr[3].strip())
                max_deg =int(arr[4].strip())
                shortest_path_len = int(arr[5].strip())
                mydict[(source, target)] = (shortest_path_len, emb_dist)
    print("Length of question feature dict", len(mydict))
    #print(mydict[("concept_journalist_dana_milbank", "concept_company_dc")])
    return mydict



if __name__ == "__main__":
    if(len(sys.argv) == 4):
        minerva_results = sys.argv[1]
        xqa_results = sys.argv[2]
        question_propeties_file = sys.argv[3]
        minerva_question_answer_feature_dict =  read_minerva_output(minerva_results)
        datatype = "nell"
        if(datatype == "nell"):
            # nell minerva test paths are directly connected, hence first 
            # predicted path is always the direct relation, which we ignore
            # consider the first predicted path by XQA which contains at least 1
            # hop
            xqa_start_column_id=11
        else:
            xqa_start_column_id=6
        xqa_question_answer_feature_dict = read_xqa_output(xqa_results,
                                                               xqa_start_column_id)
        question_properties_dict = get_question_dict(question_propeties_file)
        merged_question_minerva_xqa_results_dict = merge_dict(question_properties_dict, minerva_question_answer_feature_dict, xqa_question_answer_feature_dict)
        print(len(question_properties_dict),
              len(minerva_question_answer_feature_dict),
              len(xqa_question_answer_feature_dict),
              len(merged_question_minerva_xqa_results_dict))
        plot_minerva_comparisons_all(merged_question_minerva_xqa_results_dict)

        plot_num_successful_predicted_paths(datatype)
        #plotall(datadir)
    else:
        print("Usage minerva_processed_output_file_with_valid_paths_and_features , xqa_output_file, question.properties_file")
