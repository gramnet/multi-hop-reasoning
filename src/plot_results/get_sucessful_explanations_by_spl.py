#!/usr/bin/python
import os,sys
import numpy as np
import re
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

#@Input1: minerva outfile in format:
    #question   path    score
#@Input2: Best output model from XQA (best : num successful explanations)
    #original_path  orig_path_len   orig_path_coh  orig_path_coh2   orig_path_max_degree       (followed by for first predicted path which is to be ignored as it will pick the direct edges) followed by next_predicted_path_properties in tab format

#@Output : generate dict(question) -> (minerva_path_with_len_coh_specificity,xqa_path_with_len_coh_specificity)
# Ignore all question for which both minerva or xqa do not have an output
#@Output plot coherence, length and specificity


def plot_num_successful_predicted_paths(wikispeedia_success_counts_dict,
                                        nell_sucess_counts_dict, freebase_sucess_counts_dict):

    shortest_path_dist = wikispeedia_success_counts_dict.keys()
    wikispeedia_nums = [wikispeedia_success_counts_dict[x] for x in
                        shortest_path_dist]
    nell_nums = [nell_sucess_counts_dict[x] for x in shortest_path_dist]
    freebase_nums = [freebase_sucess_counts_dict[x] for x in shortest_path_dist]

    plt.plot(shortest_path_dist, wikispeedia_nums)
    plt.plot(shortest_path_dist, nell_nums)
    plt.plot(shortest_path_dist, freebase_nums)
    plt.legend(('wikispeedia', 'nell', 'freebase'))
    plt.xlabel("Question Pair : Shortest Path Length")
    plt.ylabel("Percent of Successful Explanations")
    plt.savefig("alldatasets_success_explanations.png")
    plt.show()


def read_xqa_output(xqa_results, start_column_index):
    mydict = {}
    print("Reading data from", xqa_results)
    with open(xqa_results, "r") as f:
        for line in f:
            all_paths_with_features = line.strip().split("\t")
            if(len(all_paths_with_features) > start_column_index):
                arr = all_paths_with_features[start_column_index: start_column_index+5]
                valid_path = arr[0][1:-1].split(",")
                nodes = [ x.strip()[1:-1] for x in valid_path]
                pathlen= int(arr[1])
                path_coherence = float(arr[3])
                path_max_degree = int(arr[4])
                mydict[(nodes[0], nodes[-1])] = (nodes, pathlen, path_coherence,
                                            path_max_degree)
    return mydict

def update_count(mydict, key):
    if(key in mydict):
        count = mydict[key]
        mydict[key] = count+1
    else:
        count = 1
# creates a merged dictionary containing only keys 
# which are present in both input dictionaries
# resulting dictioary
# common_key -> (dict1.value, dict2.value)
def count_num_success_byspl(question_feature_dict, answer_feature_dict):
    count_success_spl_dict = {}
    count_question_spl = {}
    print("Merging..", len(question_feature_dict), len(answer_feature_dict))
    for question, question_features in question_feature_dict.iteritems():
        # other dictionaries contain data aboiut this question as well
        #spl = shortest path length
        question_spl = question_features[0]
        #print(question, question_spl)
        if question_spl in count_question_spl:
            tmp = count_question_spl[question_spl]
        else:
            tmp = 0
        count_question_spl[question_spl]  = tmp + 1
        if(question in answer_feature_dict):
            #print("Found answer to question", question,
            #      answer_feature_dict[question])
            if(question_spl in count_success_spl_dict):
                tmp2 = count_success_spl_dict[question_spl] 
            else:
                tmp2 = 0
            count_success_spl_dict[question_spl]  = tmp2 + 1
    #print(len(count_question_spl), len(count_success_spl_dict))
    print(count_question_spl)
    print(count_success_spl_dict)
    return map_to_percent(count_question_spl, count_success_spl_dict)

def map_to_percent(totalcounts_dict, success_counts_dict):
    mydict = {}
    for k,v in totalcounts_dict.iteritems():
        if(k in success_counts_dict):
            success_count = success_counts_dict[k]
        else:
            success_count = 0.0
        percent_total = (success_count*100)/v
        #print(k, percent_total)
        mydict[k] = percent_total
    return mydict


def get_question_dict(question_properties_file):
    mydict = {}
    with open(question_properties_file, "r") as f:
        for line in f:
            if(not line.startswith("#")):
                arr = line.strip().split(", ")
                if(len(arr) < 6):
                    print("iNvalid line, ignoring", line)
                    continue
                source = arr[1].strip()[1:-1]
                target = arr[2].strip()[1:-1]
                emb_dist = float( arr[3].strip())
                max_deg =int(arr[4].strip())
                shortest_path_len = int(arr[5].strip())
                mydict[(source, target)] = (shortest_path_len, emb_dist)
    print("Length of question feature dict", len(mydict))
    #print(mydict[("concept_journalist_dana_milbank", "concept_company_dc")])
    return mydict



if __name__ == "__main__":
    xqa_start_column_id=5
    wikidir = "/home/d3x771/datasets/wikispeedia_paths-and-graph/"
    nelldir = "/home/d3x771/datasets/nell/"
    freebasedir = "/home/d3x771/datasets/freebase/"
    
    wiki_question_dict = get_question_dict(wikidir + "/wikispeedia.test_questions.properties.tsv")
    wiki_answer_dict = read_xqa_output(wikidir + "wikispeedia.path_comparison.nbrs20_coherence.tsv_0", xqa_start_column_id)
    wikidict = count_num_success_byspl(wiki_question_dict, wiki_answer_dict)
    print(wikidict)
    
    nell_question_dict = get_question_dict(nelldir + "/training_data_gen/nell.15K_test_questions.properties.txt")
    nell_answer_dict = read_xqa_output(nelldir + "/results/xqa_results/model_trained_entities_129K/xqa_entity_tests/path_comparison.nbrs2_coherence.tsv_0",xqa_start_column_id)
    nelldict = count_num_success_byspl(nell_question_dict, nell_answer_dict)


    freebase_question_dict = get_question_dict(freebasedir +
                                               "/freebase.test_questions.properties.txt")
    freebase_answer_dict = read_xqa_output(freebasedir +
                                           "path_comparison.freebase.nbrs10_coherence.tsv_0",
                                           xqa_start_column_id)
    freebasedict = count_num_success_byspl(freebase_question_dict,
                                          freebase_answer_dict)

    plot_num_successful_predicted_paths(wikidict, nelldict,
                                        freebasedict)
