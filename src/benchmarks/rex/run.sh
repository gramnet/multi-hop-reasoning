#!/bin/bash

#SBATCH -N 1
#SBATCH -t 47:59:00
#SBATCH -p dlt
#SBATCH --gres=gpu:1

##screen
##salloc -N 1 -t 23:59:00 -p dl --gres=gpu:2
##squeue
##ssh $node
##bash

cd /people/d3x771/projects/xqa/xqa-benchmarks/rex/
module purge
module load cuda/9.0.176
module load python/anaconda2

#datatype=metaqa
#datadir=/projects/streaming_graph/question_answering_datasets/metaqa/
#start=100000
#stop=191110
#python rex.py $datadir $datatype $start $stop

#datatype=wiki
#datadir=/projects/streaming_graph/question_answering_datasets/wikispeedia/wikispeedia_paths-and-graph/
#python rex.py $datadir $datatype

datatype=freebase
datadir=/projects/streaming_graph/question_answering_datasets/freebase/
python rex.py $datadir $datatype 

#datatype=nell
#datadir=/projects/streaming_graph/question_answering_datasets/nell_minerva/nell/
#python rex.py $datadir $datatype

#for infile in ../../config/nell/nell.cfg ../../config/wikispeedia/wikispeedia.cfg 
#for infile in ../../config/wikispreedia/wikispeedia.cfg 
#do
#		echo $infile
#		#python qa.py $infile
#		python vector_walk.py $infile
#		#python test_model.py $infile
#done
