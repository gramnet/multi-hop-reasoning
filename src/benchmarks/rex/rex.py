from collections import defaultdict
from collections import Counter
import io
import json
import sys
import time
from load_data import *

def get_node_id_type_map(node_label_id_map, node_types):
    """ 
    Helper function to read a file such as metaqa.node_types.json and return
    a dictionary mapping each node id to it's corresponding types.
    IMPORTANT: Each dictionary key maps the node id to a list.  If a node
    "Bruce Lee" has id 124 and mapped to type "Actor", then the output dict
    will have a key-value pair "124" -> ["Actor"].
    This list representation for types is important for all consuming functions.
    """
    node_id_type_map = dict()
    for node_label, node_id in node_label_id_map.items():
        if(node_label in node_types):
            node_id_type_map[str(node_id)] = node_types[node_label]
    return node_id_type_map

def expand_patterns_with_type_lists(pattern_seq):
    """
    Some nodes have multiple types.  Therefore, given a pattern as below, 
        [[u'movie'], [u'director', u'writer'], [u'movie']]
    we want to generate two sequences:
        [u'movie', u'writer', u'movie']
        [u'movie', u'director', u'movie']
    """
    patterns_to_grow = []
    # initialize the lists
    for entry in pattern_seq[0]:
        patterns_to_grow.append([entry])

    # Iterate over the remaining entries and generate new lists by 
    # performing cross products
    for i in range(1, len(pattern_seq)):
        next_level_patterns = []
        for seq in patterns_to_grow:
            for entry in pattern_seq[i]:
                new_pattern = list(seq)
                new_pattern.append(entry)
                next_level_patterns.append(new_pattern)
        patterns_to_grow = next_level_patterns
    return patterns_to_grow

def test_patterns_with_type_lists():
    print('Input pattern sequence: ')
    pattern_seq = [['movie'], ['director', 'writer'], ['movie']]
    print(pattern_seq)

    expanded_patterns = expand_patterns_with_type_lists(pattern_seq)
    assert(len(expanded_patterns) == 2)
    assert(expanded_patterns[0][0] == 'movie')
    assert(expanded_patterns[1][0] == 'movie')
    assert(expanded_patterns[0][1] == 'director')
    assert(expanded_patterns[1][1] == 'writer')
    assert(expanded_patterns[0][2] == 'movie')
    assert(expanded_patterns[1][2] == 'movie')
    print('Expanded pattern sequences:')
    print(expanded_patterns)

def get_pattern_key(node_type_sequence_based_pattern):
    """
    pattern: a list of node types such as [u'movie', u'writer', u'movie']
    output: a string concatenating the nodes with '_' such as u'movie_writer_movie'
    """
    return '_'.join(node_type_sequence_based_pattern)

def compute_pattern_distribution_by_isomorphism(random_walks_file_path, \
        node_id_type_map, \
        node_type_map_path, pattern_len, pattern_support = defaultdict(int)):
    """
    Read a random walks file typically used as node2vec.  We process each random walk
    by applying a window of length pattern_len.  The sliding window retrieves a path
    with pattern_len nodes and (pattern_len-1) edges.  Each path is mapped to a pattern
    only based on node types. Finally, a counter with the number of occurrences of each
    pattern is returned.
    """ 
    print('Processing random walk data for isomorphism-based pattern distribution...')
    t1 = time.time()
    num_errors = 0
    num_good_seqs = 0
    with open(random_walks_file_path) as f_in:
        for line in f_in:
            line = line[1:-1]
            node_ids = line.split(', ')
            for i in range(len(node_ids)-pattern_len+1):
                try:
                    path_seq = node_ids[i:i+pattern_len]
                    path_pattern = [node_id_type_map[j] for j in path_seq]
                    expanded_patterns =  expand_patterns_with_type_lists(path_pattern)
                    for p in expanded_patterns:
                        path_pattern_key = get_pattern_key(p)
                        pattern_support[path_pattern_key] += 1
                        num_good_seqs += 1
                except KeyError:
                    num_errors += 1
    t2 = time.time()
    print('Processing time: %f seconds' % (t2-t1))
    print('Number of KeyErrors: %d' % num_errors)
    print('Number of good sequences: %d' % num_good_seqs)
    return pattern_support

def compute_pattern_distribution_by_monotone_count(random_walks_file_path, 
        node_id_type_map, 
        pattern_len, pattern_support = defaultdict(int)):
   
    print('Processing random walk data for monotone count-based pattern dist...')
    t1 = time.time()
    num_errors = 0
    num_good_seqs = 0
    # Dictionary to track the pattern_node to its instance nodes in the graph
    pattern_node_map = defaultdict(dict)

    with open(random_walks_file_path) as f_in:
        for line in f_in:
            if(line.startswith('[')):
                line = line.strip()[1:-1]
                node_ids = [x.strip() for x in line.split(', ')]
            else:
                node_ids = line.strip().split(',')
            for i in range(len(node_ids)-pattern_len+1):
                try:
                    path_seq = node_ids[i:i+pattern_len]
                    #print(path_seq)
                    path_pattern = [node_id_type_map[j] for j in path_seq]
                    expanded_patterns =  expand_patterns_with_type_lists(path_pattern)
                    for candidate_pattern in expanded_patterns:
                        pattern_key = '_'.join(candidate_pattern)            
                        for j in range(len(candidate_pattern)):
                            index_key = j
                            if index_key not in pattern_node_map[pattern_key]:
                                pattern_node_map[pattern_key][index_key] = set()
                            pattern_node_map[pattern_key][index_key].add(path_seq[j])
                        num_good_seqs += 1
                except KeyError:
                    num_errors += 1

    pattern_monotone_count = dict() 
    for pattern_key, node_map in pattern_node_map.items():
        pattern_node_instance_count = []
        for pattern_node, pattern_node_instances in node_map.items():
            pattern_node_instance_count.append(len(pattern_node_instances))
        monotone_count = min(pattern_node_instance_count)
        pattern_monotone_count[pattern_key] = monotone_count

    t2 = time.time()
    print('Processing time: %f seconds' % (t2-t1))
    print('Number of KeyErrors: %d' % num_errors)
    print('Number of good sequences: %d' % num_good_seqs)

    pattern_support.update(pattern_monotone_count)
    return

def get_pattern_support(pattern_support, node_type_sequence_based_pattern):
    """
    pattern: a list of node types such as [u'movie', u'writer', u'movie']
    pattern_support: a dictionary containining supports for patterns.  Keys
            are string based signatures of patterns. For example, the above
            pattern would be encoded as u'movie_writer_movie'
    """
    pattern_key = get_pattern_key(node_type_sequence_based_pattern)
    return pattern_support[pattern_key] 

def get_path_pattern_distribution(random_walks_file, node_id_type_map, 
        measure_desc, 
        min_pattern_size_by_node_count, max_pattern_size_by_node_count):
    """
    random_walks_file_path: A file containing random walks that are typically collected
            for node2vec computations.  The random walks are captured with integer-based
            ids. See metaqa.walks.p1.q1.l20.w20.txt for example.
    node_ids_file: A json file providing mapping between node label to integer ids. The
            ids need to be consistent with ids in the random walks file. See
            metaqa.vertex_dict.json for example.
    node_types_file: A json file containing mapping between node label to a list of 
            types. For example, see metaqa.node_types.json.
    measure_desc: "count" (isomorphism based) or "monocount".
    min_pattern_size_by_node_count: Minimum size of paths to collect pattern statistics.
    max_pattern_size_by_node_count: Maximum size of paths to collect pattern statistics.
    """
    pattern_support = defaultdict(int)

    # The dictionary will be filled with pattern statistics with increasing size through
    # each pass of the following loop.  No pattern pruning is performed.  In other words,
    # we do not apply thresholding to patterns of size i to decide which patterns are 
    # "frequent" and only collect statistics for patterns of size (i+1).
    print('-----------------------------------------------------------')
    print('Computing path pattern distribution using [%s] measure ...' % measure_desc)
    print('-----------------------------------------------------------')
    t1 = time.time()
    for i in range(min_pattern_size_by_node_count, max_pattern_size_by_node_count+1):
        print(' ---> Computing for patterns with %d nodes' % i)
        if measure_desc == 'count':
            compute_pattern_distribution_by_isomorphism(random_walks_file, \
                    node_id_type_map, i, pattern_support)
        elif measure_desc == 'monocount':
            compute_pattern_distribution_by_monotone_count(random_walks_file, \
                    node_id_type_map, i, pattern_support)
            tmp_outfile = random_walks_file + ".pattern_size_" + str(i) + ".json"
            print("Completed pattern count for size , dumping json at", i, tmp_outfile)
            json_dump_unicode(pattern_support, tmp_outfile)
        else:
            print('Invalid measure description provided: %s' % measure_desc)
            print('Valid arg for get_path_pattern_distribution() is count/monocount')
            sys.exit(1) 
    t2 = time.time()
    print('Finished computing path pattern distribution in %f secs.' % (t2-t1))
    print('-----------------------------------------------------------')
    return pattern_support

def get_local_distribution(random_walks_file_path, 
        node_label_id_map_path, node_type_map_path, 
        query_src_node_id, query_dst_node_type,
        min_pattern_size_by_node_count, max_pattern_size_by_node_count):
    """
    This function searches for paths in a collection of random walks that starts
    with query_src_node_id and ends with a node whose type matches query_dst_node_type.
    For each such path, it looks up pattern_support to obtain a score for the
    corresponding pattern. Finally, it collects a distribution of such scores and
    returns the distribution.
 
    Arguments:
    random_walks_file_path: A file containing random walks that are typically collected
            for node2vec computations.  The random walks are captured with integer-based
            ids. See metaqa.walks.p1.q1.l20.w20.txt for example.
    node_label_id_map_path: A json file mapping node labels to integer ids. The
            ids need to be consistent with ids in the random walks file. See
            metaqa.vertex_dict.json for example.
    node_type_map_path: A json file containing mapping between node label to a list of 
            types. For example, see metaqa.node_types.json.
    pattern_support: A dictionary containing isomorphic counts or monotone count based
            support measures for each pattern.  Keys are string representations of a 
            pattern such as movie_actor_movie. This dictionary must be populated prior
            to calling this function.
    query_src_node_id: Node id of the start node in path explanation query.
    query_dst_node_type: Type of the destination node in path explanation query.
    min_pattern_size_by_node_count, max_pattern_size_by_node_count: Pattern sizes 
            measured in node count used to constrain the estimation of the local 
            distribution.
    """

    node_id_type_map = get_node_id_type_map(node_label_id_map_path, \
            node_type_map_path) 

    localized_score_distribution = dict(default_dict(int))
    print('Computing localized score distribution ...')

    for pattern_len in range(min_pattern_size_by_node_count, max_pattern_size_by_node_count+1):
        with open(random_walks_file_path) as f_in:
            for line in f_in:
                line = line[1:-1]
                node_ids = line.split(', ')
                for i in range(len(node_ids)-pattern_len+1):
                    try:
                        path_seq = node_ids[i:i+pattern_len]
                        # The critical block for experimenting with global or local
                        # distributions.
                        start_node = path_seq[0]
                        end_node = path_seq[-1]
                        #start_node_type = node_id_type_map[start_node]
                        end_node_type = node_id_type_map[end_node]
                        if start_node != query_src_node_id and \
                                end_node_type != query_dst_node_type:
                            continue
                        path_pattern_tmp = [node_id_type_map[j] for j in path_seq[1:-1]]
                        path_pattern_local = [start_node] + path_pattern_tmp + [end_node_type]
                        path_pattern_local_instance = [start_node] + path_pattern_tmp + [end_node]
                        expanded_patterns =  expand_patterns_with_type_lists(path_pattern_local)
                        for candidate_pattern in expanded_patterns:
                            pattern_key = '_'.join(candidate_pattern)            
                            # Need to find distribution of the explanation patterns 
                            #pattern_score = pattern_support[pattern_key]
                            localized_score_distribution[pattern_key][path_pattern_local_instance] += 1
                            #localized_score_distribution[pattern_score] += 1
                    except KeyError:
                        pass
    
    local_distribution = compile_local_distrbution_per_pattern_instance(localized_score_distribution)
    sorted_local_distributon_per_pattern = {}
    for pattern, instance_count_dict in local_distribution.iteritems():
        sorted_local_distribution = []
        for p_score in sorted(instance_count_dict.keys()):
            sorted_local_distribution.append((p_score, instance_count_dict[p_score]))
        sorted_local_distributon_per_pattern[pattern] = sorted_local_distribution
    return sorted_local_distributon_per_pattern

    #sorted_loacl_distribution = []
    #TODO: Khushbu
    # 1) add length as one of the pattern keys , creating {(length, p_score) :pattern}
    # 2) sort by length as primary key and then p_score as secondary key
    # 3) return sorted patterns
    #for p_score in sorted(local_distribution.keys()):
    #for p_score in sorted(localized_score_distribution.keys()):
    #    sorted_local_distribution.append((p_score, localized_score_distribution[p_score]))  
    #return sorted_local_distribution

def compile_local_distrbution_per_pattern_instance(localized_score_distribution):
    """
    This function takes as input a nested dictionary containing following:
        dict{pattern} = dict{pattern_count : number_of_instances}
    For example consider query (Brad_pitt, Angelina_Jolie).  Let candidate
    explanation patterns be:
        Brad_Pitt->movie->actor
        Brad_Pitt->movie->director->movie->actor
    Given Input localized_score_distribution dictionary for each pattern
    {
    "Brad_Pitt->movie->actor" : {Brad_Pitt->movie->Angelina_Jolie:1, Brad_Pitt->movie->George_Clooney:3, Brad_Pitt->movie->Jennifer_Aniston:1...},
    "Brad_Pitt->movie->director->movie->actor" : {Brad_Pitt->movie->director->movie->Angelina_Jolie:instance_count,...}
    ...
    }

    Compute as Output:
    {
    "Brad_Pitt->movie->actor" : {1:2, 3:1},
    "Brad_Pitt->movie->director->movie->actor" : {instance_count : number_of_destination_instances_with_this_count...},
    ...
    }

    """
    local_distribution = dict(default_dict(int))
    for pattern_key, pattern_instance_counts in localized_score_distribution.iteritems():
        for pattern_instance, instance_count in pattern_instance_counts.iteritems():
            local_distribution[pattern_key][instance_count] += 1
    return local_distribution

class REX:
    def __init__(self, graph, node_types_dict, monocount_dist, max_length_path_node_count):
        self.graph = graph
        self.node_types = node_types_dict
        self.monocount_dist = monocount_dist
        self.max_length_path_node_count = max_length_path_node_count
        self.sorted_patterns = self.get_sorted_pattern_per_question()

    def get_sorted_pattern_per_question(self):
        qa_pattern_attributes = dict()
        for pattern_key, count in self.monocount_dist.iteritems():
            pattern = pattern_key.split("_")
            pattern_length = len(pattern)
            question_pattern = (pattern[0], pattern[-1])
            if question_pattern in qa_pattern_attributes:
                qa_pattern_attributes[question_pattern].append((pattern_length, count, pattern))
            else:
                qa_pattern_attributes[question_pattern] = [(pattern_length, count, pattern)]
        
        sorted_pattern_per_question = dict()
        for question, answer_pattern_attr in qa_pattern_attributes.iteritems():
            sorted_patterns = sorted(answer_pattern_attr, key=lambda x: (x[0], -x[1]))
            sorted_pattern_per_question[question] = sorted_patterns
        return sorted_pattern_per_question

    def generate_paths(self, source, dest, topk):
        source_types = self.node_types[source]
        dest_types = self.node_types[dest]
        all_paths = []
        try:
            for source_type in source_types:
                for dest_type in dest_types:
                    qp_key = (source_type, dest_type)
                    #print("Looking for key :", qp_key)
                    my_patterns = self.sorted_patterns[qp_key]
                    num_patterns_to_print = min(topk, len(my_patterns))
                    #print("Topk patterns for question", source, dest,
                    #    my_patterns[num_patterns_to_print])
                    paths = self.generate_topk_paths_by_pattern(source, dest, my_patterns, topk)
                    all_paths.extend(paths)
                    if(len(all_paths) >= topk):
                        return all_paths
            return all_paths
        except KeyError:
            print("Warning : No Known patterns...IMPLEMENT random walk")
            #return self.generate_random_paths()

    def generate_topk_paths_by_pattern(self,source, dest, my_patterns, topk):
        total_paths = 0
        all_paths = []
        for pattern_with_attr in my_patterns:
            pattern = pattern_with_attr[2]
            paths = self.generate_path_by_pattern(source, dest, pattern, topk)
            all_paths.extend(paths)
            total_paths += len(paths)
            if(total_paths >= topk):
                return all_paths[0:topk]
        return all_paths

    def get_nbrs_of_type(self, source, nbr_type):
        all_nbrs = list(nx.all_neighbors(self.graph, source))
        valid_nbrs = [x for x in all_nbrs if nbr_type in self.node_types[x]]
        return valid_nbrs


    """
    Generate at MOST topk paths matching the pattern (may be less if not enough
    exists or more for pattern_length =3 (2 hop)
    """
    def generate_path_by_pattern(self, source, target, pattern, topk):
        hops = len(pattern) - 1
        paths = []
        if(hops == 2):
            common_nbrs = set(self.get_nbrs_of_type(source, pattern[1])) & \
                    set(self.get_nbrs_of_type(target, pattern[1]))
            if len(common_nbrs) > 0:
                paths = [[source, nbr, target] for nbr in common_nbrs]
        elif(hops == 3):
            src_matching_nbrs = set(self.get_nbrs_of_type(source, pattern[1]))
            dest_matching_nbrs = set(self.get_nbrs_of_type(target, pattern[2]))
            paths = []
            for src_nbr in src_matching_nbrs:
                src_nbr_hop2_cands = [x for x in nx.all_neighbors(self.graph, src_nbr)]
                valid_cands_hop2 = set(src_nbr_hop2_cands).intersection(dest_matching_nbrs)
                for cand in valid_cands_hop2:
                    path = [source, src_nbr, cand, target]
                    paths.append(path)
                    if(len(paths) >= topk): 
                        return paths

        elif(hops == 4):
            src_matching_nbrs = set(self.get_nbrs_of_type(source, pattern[1]))
            dest_matching_nbrs = set(self.get_nbrs_of_type(target, pattern[3]))
            paths = []
            for src_nbr in src_matching_nbrs:
                for dest_nbr in dest_matching_nbrs:
                    common_nbrs_middle = set(self.get_nbrs_of_type(src_nbr, pattern[2])) & \
                            set(self.get_nbrs_of_type(dest_nbr, pattern[2]))
                    for x in common_nbrs_middle:
                        path = [source, src_nbr, x , dest_nbr, target]
                        paths.append(path)
                        if(len(paths) >= topk):
                            return paths
        else:
            print("Do not support path generation of length",  len(pattern))
        
        return paths

def rex_path_generation(graph, node_types_dict, monocount_dist, question_pairs,
                        topk, outfile, sorted_pattern_outfile,
                        start, stop,
                        max_length_path_node_count=5):
    """
    This function generates paths for given question pairs using top-k patterns
    identified using monocount metric in REX paper
    Inputs 
    1) a networkx graph (nodes are stored using the node labels )
    2) The monocount dict generated earlier containing "pattern_string : count"
    3) list of question pair (question = pair of node labels)
    Output:
        Generate path matching topk-pattern for gievn question node-types
        topk-pattern are defined by (min_length, max_pattern_monocount)
    """
    rex = REX(graph, node_types_dict, monocount_dist, max_length_path_node_count)
    str_sorted_patterns = dict()
    for k,v in rex.sorted_patterns.iteritems():
        str_sorted_patterns[k[0] + "_" + k[1]] = v
    json_dump_unicode(str_sorted_patterns, sorted_pattern_outfile)
    qa_dict = dict()
    print("Generating paths for questions ", start, stop)
    for qp in question_pairs[start:stop]:
        #print("Generating paths for ", qp)
        valid_paths = rex.generate_paths(qp[0], qp[1], topk)
        qa_dict[qp[0] + "," + qp[1]] = valid_paths
    print("All paths saved to ", outfile)
    json_dump_unicode(qa_dict, outfile)
    return

def json_dump_unicode(mydict, outfile):
    with io.open(outfile, "w", encoding='utf8') as json_file:
        data = json.dumps(mydict, ensure_ascii=False, indent=4)
        json_file.write(unicode(data))

def generate_monocount_pattern_distribution(random_walks_file, node_id_type_map, 
                                            monocount_dist_outfile):

    #pattern_dist1 = get_path_pattern_distribution(random_walks_file, \
         #node_id_type_map, "count", 3, 5)
    print('Computing monocount ...')
    pattern_dist_monocount = get_path_pattern_distribution(random_walks_file, \
            node_id_type_map, "monocount", 3, 5)
    print('Number of patterns based on monocount: %d' %len(pattern_dist_monocount))
    json_dump_unicode(pattern_dist_monocount, monocount_dist_outfile)

def generate_local_pattern_distribution(random_walks_file, node_types_file,
                                  node_ids_file, local_dist_outfile, qp_list):
    node_ids = json.load(open(node_ids_file))
    node_types = json.load(open(node_types_file))

    qp_list = [['18374', 'director']]
    sorted_local_distribution = dict()

    for query in qp_list:
        print('Computing local distribution ...')
        query_src_node_id = query[0] #node_ids[query[0]]
        query_dst_node_type = query[1] #node_types[query[1]]
        sorted_local_distribution_per_pattern = get_local_distribution(random_walks_file, 
                                                                       node_ids_file, node_types_file, 
                                                                       query_src_node_id, query_dst_node_type, 3, 5)
        sorted_local_distribution[query[0] + "," + query_dst_node_type] = sorted_local_distribution_per_pattern
    json_dump_unicode(sorted_local_distribution, local_dist_outfile)
    return

def run_metaqa(datadir, start, stop):
    graph_file = datadir + '/kb.txt'
    node_ids_file = datadir + '/vertex_dict.json'
    node_types_file = datadir + '/node_types.json'
    test_qp_file = datadir + '/3-hop-vanilla/qa_test_qp.json'
    random_walks_file = datadir + '/walks/metaqa.walks.p1.q1.l20.w20'
    
    dataobj = metaqa()
    graph = dataobj.load_graph(graph_file)
    node_types_dict = dataobj.load_node_label_categories_map(node_types_file)
    node_id_dict = json.load(open(node_ids_file, "r"))
    node_id_type_map = get_node_id_type_map(node_id_dict, node_types_dict)
    qp_list = dataobj.load_test_qp(test_qp_file)
    
    # Generate monocount distribution of patterns in the graph (pattern_node_count = 3, 4 and 5)
    monocount_dist_outfile = datadir + '/rex_setup/metaqa.pattern_monocount.learned_p1.q1.l20.w20.json'
    generate_monocount_pattern_distribution(random_walks_file, node_id_type_map,
                                  monocount_dist_outfile)
    monocount_dist = json.load(open(monocount_dist_outfile, "r"))
    
    # generate paths for each question using pattern monocount and length as
    # guiding metrics
    topk = 10
    max_length_path_node_count = 5
    path_outfile = datadir + '/rex_setup/metaqa.rex.' + str(start) + '.' + str(stop) + '.paths_by_length_and_monocount.json'
    sorted_pattern_outfile = datadir + '/rex_setup/metaqa.rex.sorted_pattern_by_length_and_monocount.json'
    rex_path_generation(graph, node_types_dict, monocount_dist, qp_list,
                        topk, path_outfile, sorted_pattern_outfile, 
                        start, stop, max_length_path_node_count=5)

def get_unique_test_questions(test_paths):
    test_list = []
    for p in test_paths:
        test_list.append((p[0], p[-1]))
    return list(set(test_list))

def run_wiki(datadir):
    graph_file = datadir + '/orig/links.tsv'
    node_ids_file = datadir + '/int/vertexDictionary.out'
    node_types_file = datadir + '/orig/categories.tsv'
    test_qp_file = datadir + '/orig/training_data_gen/easy_or_short_paths/paths_finished.easy_or_short.test.tsv'
    random_walks_file = datadir + '/int/walks/wikispeedia.walks.p1.q1.l10.w20'
    
    dataobj = wikispeedia()
    graph = dataobj.load_graph(graph_file)
    node_types_dict = dataobj.load_node_label_categories_map(node_types_file)
    node_id_map = dataobj.load_vertex_dict(node_ids_file)
    node_id_type_map = get_node_id_type_map(node_id_map, node_types_dict)
    test_paths = dataobj.load_paths(test_qp_file, use_mpi=False, size=1, rank=0)
    qp_list = get_unique_test_questions(test_paths)
    
    # Generate monocount distribution of patterns in the graph (pattern_node_count = 3, 4 and 5)
    monocount_dist_outfile = datadir + '/results/rex/metaqa.pattern_monocount.learned_p1.q1.l20.w20.json'
    print("Loaded data, generating pattern monocounts...")
    generate_monocount_pattern_distribution(random_walks_file, node_id_type_map, monocount_dist_outfile)
    monocount_dist = json.load(open(monocount_dist_outfile, "r"))
    
    # generate paths for each question using pattern monocount and length as
    # guiding metrics
    topk = 10
    max_length_path_node_count = 5
    path_outfile = datadir + '/results/rex/wiki.rex.' + '.paths_by_length_and_monocount.json'
    sorted_pattern_outfile = datadir + '/results/rex/wiki.rex.sorted_pattern_by_length_and_monocount.json'
    rex_path_generation(graph, node_types_dict, monocount_dist, qp_list,
                        topk, path_outfile, sorted_pattern_outfile, 
                        start=0, stop=len(qp_list), max_length_path_node_count=5)

def run_freebase(datadir):
    graph_file = datadir + '/graph/allEdges.txt'
    node_ids_file = datadir + '/vertexDictionary.out'
    node_types_file = datadir + '/node_categories.txt'
    test_qp_file = datadir + '/minerva_setup/test.txt'
    random_walks_file = datadir + '/walks/freebase.walks.p1.q1.l40.w20'
    
    dataobj = freebase()
    graph = dataobj.load_graph(graph_file)
    node_types_dict = dataobj.load_node_label_categories_map(node_types_file)
    node_id_map = dataobj.load_vertex_dict(node_ids_file)
    node_id_type_map = get_node_id_type_map(node_id_map, node_types_dict)
    test_paths = dataobj.load_paths(test_qp_file, use_mpi=False, size=1, rank=0)
    qp_list = get_unique_test_questions(test_paths)
    
    # Generate monocount distribution of patterns in the graph (pattern_node_count = 3, 4 and 5)
    monocount_dist_outfile = datadir + '/rex/freebase.pattern_monocount.learned_p1.q1.l40.w20.json'
    print("Created node_id_type_map", len(node_id_type_map))
    #print(node_id_type_map)
    print("Loaded data, generating pattern monocounts...")
    generate_monocount_pattern_distribution(random_walks_file, node_id_type_map, monocount_dist_outfile)
    monocount_dist = json.load(open(monocount_dist_outfile, "r"))
    
    # generate paths for each question using pattern monocount and length as
    # guiding metrics
    topk = 10
    max_length_path_node_count = 5
    path_outfile = datadir + '/rex/freebase.rex.paths_by_length_and_monocount.json'
    sorted_pattern_outfile = datadir + '/rex/freebase.rex.sorted_pattern_by_length_and_monocount.json'
    rex_path_generation(graph, node_types_dict, monocount_dist, qp_list,
                        topk, path_outfile, sorted_pattern_outfile, 
                        start=0, stop=len(qp_list), max_length_path_node_count=5)

def run_nell(datadir):
    graph_file = datadir + '/graph.txt'
    node_ids_file = datadir + '/vertex_dict.json'
    node_types_file = datadir + '/node_types.json'
    test_qp_file = datadir + '3-hop-vanilla/qa_test_qp.json'
    random_walks_file = datadir + '/walks/metaqa.walks.p1.q1.l20.w20.txt'
    
    dataobj = metaqa()
    graph = dataobj.load_graph(graph_file)
    node_types_dict = dataobj.load_node_label_categories_map(node_types_file)
    node_id_map = dataobj.load_vertex_dict(node_ids_file)
    node_id_type_map = get_node_id_type_map(node_id_map, node_types_dict)
    test_paths = dataobj.load_paths(test_qp_file, use_mpi=False, size=1, rank=0)
    qp_list = get_unique_test_questions(test_paths)
    
    # Generate monocount distribution of patterns in the graph (pattern_node_count = 3, 4 and 5)
    monocount_dist_outfile = datadir + '/rex/nell.pattern_monocount.learned_p1.q1.l20.w20.json'
    generate_monocount_pattern_distribution(random_walks_file, node_id_type_map, monocount_dist_outfile)
    monocount_dist = json.load(open(monocount_dist_outfile, "r"))
    
    # generate paths for each question using pattern monocount and length as
    # guiding metrics
    topk = 10
    max_length_path_node_count = 5
    path_outfile = datadir + '/rex/nell.rex.paths_by_length_and_monocount.json'
    sorted_pattern_outfile = datadir + '/rex/nell.rex.sorted_pattern_by_length_and_monocount.json'
    rex_path_generation(graph, node_types_dict, monocount_dist, qp_list,
                        topk, path_outfile, sorted_pattern_outfile, 
                        start=0, stop=len(qp_list), max_length_path_node_count=5)





if __name__ == "__main__":
    if(len(sys.argv) < 3):
        print("Usage <datadir> <data type>")
        sys.exit(1)
    datadir = sys.argv[1]
    datatype = sys.argv[2]
    
    if(datatype == 'metaqa'):
        if(len(sys.argv) < 5):
            print("Usage <datadir> <data type> <start-index-test> <stop-idex-test>")
            sys.exit(1)
        start = int(sys.argv[3])
        stop = int(sys.argv[4])
        run_metaqa(datadir, start, stop)
    elif(datatype == 'wiki'):
        run_wiki(datadir)
    elif(datatype == 'freebase'):
        run_freebase(datadir)
    elif(datatype == 'nell'):
        run_nell(datadir)

    #local_dist_outfile = datadir + '/rex_setup/metaqa.pattern_local_distribution.learned_p1.q1.l20.w20.json'
    #generate_local_pattern_distribution(random_walks_file, node_types_file,
    #                              node_ids_file, local_dist_outfile, qp_list)
    #rex_path_generation(graph, node_types_dict, monocount_dist, qp_list,
    #                    topk, path_outfile, sorted_pattern_outfile, max_length_path_node_count=5)

