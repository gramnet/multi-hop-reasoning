from collections import defaultdict
from collections import Counter
from typing import List
from typing import NewType
import json

Path = NewType('Path', List[str])

##class Triple:
##    def __init__(self, s, p, o):
##        self.sub = s
##        self.pred = p
##        self.obj = o
##        return
##
##class PathPattern:
##    def __init__(self, path: list[Triple]):
##        self.pattern_edge_list = []
##        self.node_map = dict()
##        for i in range(path):
##            sub = 'v_%d' % i
##            obj = 'v_%d' % (i+1)
##            self.node_map[sub] = path[i].sub
##            pred = path[i].pred
##            self.node_map[obj] = path[i].obj
##            triple = Triple(sub, pred, obj)
##            self.pattern_edge_list.append(triple)
##        return
##
##    def __len__(self):
##        return len(self.pattern_edge_list)
##    
##    def key(self) -> str:
##        return '_'.join(map(lambda e:e.pred, self.pattern_edge_list))
##
##def get_monotone_count(graph: Graph, v_s: str, v_t: str, 
##        path_list: list[Path]) -> list[Path]:
##    pattern_node_map = defaultdict(defaultdict(set))
##    for p in path_list:
##        pattern = Path()
##        node_map = dict()
##        pattern: Path, node_map: dict = get_pattern(p)
##        for pattern_node, instance_node in node_map.items():
##            pattern_node_map[pattern.get_key()][pattern_node].add(instance_node)
##
##    pattern_monotone_count = dict() 
##    for pattern_key, node_map in pattern_node_map.items():
##        pattern_node_instance_count = []
##        for pattern_node, pattern_node_instances in node_map.items():
##            pattern_node_instance_count.append(len(pattern_node_instances))
##        monotone_count = min(pattern_node_instance_count)
##        pattern_monotone_count[pattern_key] = monotone_count
##        
##def path_enum(graph: Graph, v_s: str, v_t: str) -> list[Path]:
##    path_list = []
##    graph_traversal_results = graph_traversal.get_walks(graph, v_s, v_t)
##    TODO_map_traversal_results_to_path_list
##

def score_path(path: Path) -> int:
    return len(path)

def get_paths_by_entity_pairs(path_file: str) -> List[Path]:
    paths_by_entity_pairs = defaultdict(list)
    with open(path_file) as f_in:
        for line in f_in:
            path = line.strip()[1:-1].split(', ')
            path = [s[1:-1] for s in path]
            key = '%s,%s' % (path[0], path[-1])
            paths_by_entity_pairs[key].append(path)
    print('Number of pairs: %d' % len(paths_by_entity_pairs))

##    path_count_distribution = Counter()
##    for entity_pair, path_list in paths_by_entity_pairs.items():
##        path_count_distribution[len(path_list)] += 1
##    print('Path count distribution -> ')
##    print(path_count_distribution)
    return paths_by_entity_pairs

def rank_paths_by_entity_pairs(paths_by_entity_pairs, k):
    ranked_paths_by_entity_pairs = dict()
    for entity_pair, list_of_paths in paths_by_entity_pairs.items():
        scores_and_paths = [(score_path(p), p) for p in list_of_paths]
        ordered_scores_and_paths = sorted(scores_and_paths, \
                key=lambda score_path_pair: score_path_pair[0])
        scored_paths = [s_p[1] for s_p in ordered_scores_and_paths]
        if len(scored_paths) >= k:
            ranked_paths_by_entity_pairs[entity_pair] = scored_paths[:k]
    return ranked_paths_by_entity_pairs

def store_topk_paths_by_entity_pairs(ranked_paths_by_entity_pairs, out_file):
    with open(out_file, 'w') as f_out:
        json.dump(ranked_paths_by_entity_pairs, f_out, sort_keys=True, indent=4)
    return
                    
if __name__ == '__main__':
    k = 5
    path_file = 'entity_pairs.tsv.plen5.paths'
    paths_by_entity_pairs = get_paths_by_entity_pairs(path_file)
    ranked_paths_by_entity_pairs = rank_paths_by_entity_pairs(paths_by_entity_pairs, k)
    out_file = '%s.top_%d.json' % (path_file, k)
    store_topk_paths_by_entity_pairs(ranked_paths_by_entity_pairs, out_file)
    
