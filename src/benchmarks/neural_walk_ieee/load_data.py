import numpy as np
import networkx as nx
import io
import sys
import json
import configparser

class yago:
  def load_node_label_embedding_map(self, node_embedding_path):
    print('Loading node embeddings from [' + node_embedding_path + ']')
    f = open(node_embedding_path)
    node_embeddings = dict()
    for line in f:
      tokens = line.strip().split(";;;;")
      if(len(tokens) == 2):
        node_label = tokens[0]
        emb = tokens[1]
        v = emb[1:-1].split(", ")
        cleanv = [float(i[1:-1]) for i in v]
        #node_embeddings[node_label] = np.array(cleanv, np.float)
        node_embeddings[node_label] = cleanv
    f.close()  
    return node_embeddings
  
  def load_node_label_categories_map(self, node_category_path):
    print('Loading node categories from [' + node_category_path + ']')
    f = open(node_category_path, "r")
    mydict = {}
    for line in f:
      tokens = line.strip().split("\t")
      if(len(tokens) == 2):
        mydict[tokens[0]] = tokens[1]
    return mydict

  def load_degree(self, degree_path):
    print("Loading node degrees from", degree_path)
    f = open(degree_path)
    node_degrees = {}
    for line in f:
      tokens = line.strip().split("\t")
      if(len(tokens) == 2 and not line.startswith('#')):
        node_degrees[tokens[0]] = int(tokens[1])
    f.close()
    return node_degrees

  def load_paths(self, yago_path_file, use_mpi, size, rank):
    print('Loading paths from [' + yago_path_file + ']')
    f = open(yago_path_file)
    paths = []
    count = 0
    print('USE_MPI = ' + str(use_mpi))
    for line in f:
      if line[0] == '#':
        continue
      if (use_mpi and (count % size == rank)) or (use_mpi == False):
        path = line.strip().split(',')
        paths.append(path)
      count += 1
    f.close()
    print('Number of paths = ' + str(len(paths)))
    return paths
  
  def load_graph(self, path):
    f_in = open(path)
    graph = nx.Graph()
    for line in f_in:
      line = line.strip()
      if len(line) == 0 or line[0] == '#':
        continue
      [u, v] = line.strip().split(' ')
      graph.add_edge(u, v)
    f_in.close()
    return graph

  def get_nodes_from_answer_user_study(self, path):
    print("Found yago paths")
    nodes_with_relations = path.split(" ")
    path_seq = []
    for n in nodes_with_relations:
      if(n not in path_seq and not n.startswith("__(")):
        path_seq.append(n)
    return path_seq



class wikispeedia:
  def load_node_label_embedding_map(self, node_embedding_path):
    print('Loading node embeddings from [' + node_embedding_path + ']')
    f = open(node_embedding_path)
    node_embeddings = dict()
    line_count = 0
    for line in f:
      line_count += 1
      # ignore the header
      if line_count == 1:
        continue 
      tokens = line.strip().split()
      node_label = tokens[0]
      embedding_vector = tokens[1:] #np.asarray(tokens[1:], dtype='float32')
      node_embeddings[node_label] = embedding_vector
    f.close()  
    return node_embeddings

  def load_node_label_categories_map(self, node_category_path):
    print('Loading node categories from [' + node_category_path + ']')
    f = open(node_category_path, "r")
    mydict = {}
    for line in f:
      tokens = line.strip().split("\t")
      if(len(tokens) == 2):
        mydict[tokens[0]] = tokens[1]
    return mydict

  def load_degree(self, degree_path):
    print("Loading node degrees from", degree_path)
    f = open(degree_path)
    node_degrees = {}
    for line in f:
      tokens = line.strip().split("\t")
      if(len(tokens) == 2):
        node_degrees[tokens[0]] = int(tokens[1])
    f.close()
    return node_degrees

  def load_paths(self, wikispeedia_path_file, use_mpi, size, rank):
    print('Loading paths from [' + wikispeedia_path_file + ']')
    f = open(wikispeedia_path_file)
    paths = []
    count = 0
    print('USE_MPI = ' + str(use_mpi))
    for line in f:
      if (use_mpi and (count % size == rank)) or (use_mpi == False):
        if line[0] == '#' or len(line) < 10:
          continue
        tokens = line.split('\t')
        if(len(tokens) > 1):
          path = tokens[3]
        else:
          path = line.strip()
        #print(path)
        nodes = path.split(';')
        final_seq = []    
        #backtrack = False
        for n in nodes:
          if n == '<':
            final_seq.pop()
            #backtrack = True
          else:
            final_seq.append(n)
        paths.append(final_seq)
      count += 1
    f.close()
    print('Number of paths = ' + str(len(paths)))
    return paths

  def load_graph(self, path):
    f_in = open(path)
    graph = nx.Graph()
    for line in f_in:
      line = line.strip()
      if len(line) == 0 or line[0] == '#':
        continue
      [u, v] = line.strip().split('\t')
      graph.add_edge(u, v)
    f_in.close()
    #nbrs = ' | '.join(nx.all_neighbors(graph, 'Barack_Obama'))
    #nbrs = ' | '.join(nx.all_neighbors(graph, 'Google'))
    #print(nbrs)
    return graph

  def get_nodes_from_answer_user_study(self, path):
    return path.split(";")
    
class freebase:
  def load_node_label_embedding_map(self, node_embedding_path):
    print('Loading node embeddings from [' + node_embedding_path + ']')
    f = open(node_embedding_path)
    node_embeddings = dict()
    line_count = 0
    for line in f:
      line_count += 1
      # ignore the header
      if line_count == 1:
        continue 
      tokens = line.strip().split()
      node_label = tokens[0]
      embedding_vector = tokens[1:] #np.asarray(tokens[1:], dtype='float32')
      node_embeddings[node_label] = embedding_vector
    f.close()  
    return node_embeddings

  def load_node_label_categories_map(self, node_category_path):
    print('Loading node categories from [' + node_category_path + ']')
    f = open(node_category_path, "r")
    mydict = {}
    for line in f:
      tokens = line.strip().split("\t")
      if(len(tokens) == 2):
        mydict[tokens[0].strip()] = tokens[1].strip()
    return mydict

  def load_degree(self, degree_path):
    print("Loading node degrees from", degree_path)
    f = open(degree_path)
    node_degrees = {}
    for line in f:
      tokens = line.strip().split("\t")
      if(len(tokens) == 2):
        node_degrees[tokens[0]] = int(tokens[1])
    f.close()
    return node_degrees

  def load_paths(self, wikispeedia_path_file, use_mpi, size, rank):
    print('Loading paths from [' + wikispeedia_path_file + ']')
    f = open(wikispeedia_path_file)
    paths = []
    count = 0
    print('USE_MPI = ' + str(use_mpi))
    for line in f:
      if (use_mpi and (count % size == rank)) or (use_mpi == False):
        if line[0] == '#':
          continue
        nodes = line.strip().split(';')
        paths.append(nodes)
      count += 1
    f.close()
    print('Number of paths = ' + str(len(paths)))
    return paths

  def load_graph(self, path):
    f_in = open(path)
    G = nx.Graph()
    for line in f_in:
      if len(line) == 0 or line[0] == '#':
        continue
      arr = line.strip().split('\t')
      if(len(arr) == 3):
        G.add_edge(arr[0], arr[2], label=arr[1])
    print(G.number_of_nodes(), G.number_of_edges())
    f_in.close()
    return G

  def get_nodes_from_answer_user_study(self, path):
    nodes = path.split(", ")
    cleaned_nodes = [x[1:-1] for x in nodes] # remove leading and trailing '
    return cleaned_nodes
   

class nell:
  def load_node_label_embedding_map(self, node_embedding_path):
    print('Loading node embeddings from [' + node_embedding_path + ']')
    f = open(node_embedding_path)
    node_embeddings = dict()
    line_count = 0
    for line in f:
      line_count += 1
      # ignore the header
      if line_count == 1:
          continue
      tokens = line.strip().split("\t")
      node_label = tokens[0]
      embedding_vector = tokens[1:] #np.asarray(tokens[1:], dtype='float32')
      node_embeddings[node_label] = embedding_vector
    f.close()  
    return node_embeddings

  def load_node_label_categories_map(self, node_category_path):
    print('Loading node categories from [' + node_category_path + ']')
    f = open(node_category_path, "r")
    mydict = {}
    for line in f:
      tokens = line.strip().split("\t")
      if(len(tokens) == 2):
        mydict[tokens[0].strip()] = tokens[1].strip()
    return mydict

  def load_degree(self, degree_path):
    print("Loading node degrees from", degree_path)
    f = open(degree_path)
    node_degrees = {}
    for line in f:
      tokens = line.strip().split("\t")
      if(len(tokens) == 2):
        node_degrees[tokens[0]] = int(tokens[1])
    f.close()
    return node_degrees

  def load_paths(self, wikispeedia_path_file, use_mpi, size, rank):
    print('Loading paths from [' + wikispeedia_path_file + ']')
    f = open(wikispeedia_path_file)
    paths = []
    count = 0
    print('USE_MPI = ' + str(use_mpi))
    for line in f:
      if (use_mpi and (count % size == rank)) or (use_mpi == False):
        if line[0] == '#':
          continue
        nodes = line.strip().split(';')
        paths.append(nodes)
      count += 1
    f.close()
    print('Number of paths = ' + str(len(paths)))
    return paths

  def load_graph(self, path):
    f_in = open(path)
    G = nx.Graph()
    for line in f_in:
      if len(line) == 0 or line[0] == '#':
        continue
      arr = line.strip().split('\t')
      if(len(arr) == 3):
        G.add_edge(arr[0], arr[2], label=arr[1])
    print(G.number_of_nodes(), G.number_of_edges())
    f_in.close()
    return G

  def get_nodes_from_answer_user_study(self, path):
    nodes = path.split(", ")
    cleaned_nodes = [x[1:-1] for x in nodes] # remove leading and trailing '
    return cleaned_nodes

class metaqa:
    #returns a networkx graph
    def load_graph(self, filename, sep='|'):
        G = nx.MultiGraph()
        with io.open(filename, "r", encoding='utf8') as f:
            for line in f:
                arr = line.strip().split(sep)
                G.add_edge(arr[0], arr[2], label=arr[1])
        return G
    
    def load_paths(self, path_file, use_mpi, size, rank):
        data = json.load(io.open(path_file, 'r', encoding='utf8'))
        paths = []
        for question_paths in data.values():
            for path in question_paths:
                paths.append(path)
        return paths
     
     #returns a dict{node_label, [embeddings array of strings]}
    def load_node_label_embedding_map(self, node_embedding_path):
        print('Loading node embeddings from [' + node_embedding_path + ']')
        f = io.open(node_embedding_path, 'r', encoding='utf8')
        node_embeddings = dict()
        f.readline()
        for line in f:
            tokens = line.strip().split("\t")
            node_label = tokens[0].strip()
            embedding_vector = tokens[1:] #np.asarray(tokens[1:], dtype='float32')
            node_embeddings[node_label] = embedding_vector
        f.close()  
        return node_embeddings

     # returns a {node_label :[list of associated types]}
    def load_node_label_categories_map(self, node_category_path):
        return json.load(io.open(node_category_path, 'r', encoding='utf8'))

    # returns a dict {node_label : node_degree}
    def load_degree(self, degree_path):
        return json.load(io.open(degree_path, 'r', encoding='utf8'))





    
