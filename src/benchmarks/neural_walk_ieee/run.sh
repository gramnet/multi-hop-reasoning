#!/bin/bash

#SBATCH -N 1
#SBATCH -t 23:59:00
##SBATCH -p shared
##SBATCH --gres=gpu:1
##SBATCH -p dlv
##SBATCH --gres=gpu:2

##screen
##salloc -N 1 -t 23:59:00 -p gpu
##squeue
##ssh $node
##bash
cd /people/d3x771/projects/xqa/xqa-benchmarks/neural_walk_ieee/
config_dir=/people/d3x771/projects/xqa/xqa/config/
#infile=$1
#module load cuda/9.0.176
module load python/anaconda3

#for infile in ../../config/nell/nell.cfg ../../config/wikispeedia/wikispeedia.cfg 
#for infile in $config_dir/wikispeedia/wikispeedia.cfg $config_dir/nell/nell.cfg $config_dir/freebase/freebase.cfg 
#for infile in $config_dir/freebase/freebase.cfg 
for infile in $config_dir/metaqa/metaqa.cfg
do
		echo $infile
		python /people/d3x771/projects/xqa/xqa-benchmarks/neural_walk_ieee/main.py $infile neural
done


