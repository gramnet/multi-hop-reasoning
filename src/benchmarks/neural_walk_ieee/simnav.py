#!/usr/bin/python

import os
import sys
import numpy as np
import networkx as nx
import scipy.spatial.distance as distance
import sklearn.neighbors 
import load_data

class EmbNav:

    def __init__(self, graph, node_embeddings, emb_size, _lambda):
        self.graph = graph
        self.embeddings = node_embeddings
        self.emb_size = emb_size
        self._lambda = _lambda
        self.kdtree = CustomKDTree(node_embeddings, emb_size)


    # returns appended path if v->u path is explorable  or False otherwise
    def sim_nav(self, v, u, path, max_hop, k):
        path.append(v)
        nbrs = nx.all_neighbors(self.graph, v)
        source_neighbor_list = [n for n in nbrs]
        if (len(source_neighbor_list) == 0 or len(path) > max_hop):
            return False # no path found from v to u
        if u in source_neighbor_list:
            path.append(u)
            return path
        v_prime = self.get_nearest_nbr_cosine(u, source_neighbor_list)
        return self.sim_nav(v_prime, u, path, max_hop, k)
    
    # Finds nearest neighbour to target by embedding distanace
    # Embedding distance is calculated using cosine similarity
    def get_nearest_nbr_cosine(self, target, neighbor_list):
        neighbor_distances = [self.emb_distance(n, target) for n in
                              neighbor_list]
        neighbors_by_dist = [neighbor_list[i] for i in
                             np.argsort(neighbor_distances)]
        return neighbors_by_dist[0]


    # returns appended path if v->u path is explorable  or False otherwise
    def neural_nav(self, v, u, path, max_hop, k):
        path.append(v)
        if(v == u):
            return path
        nbrs = nx.all_neighbors(self.graph, v)
        source_neighbor_list = [n for n in nbrs]
        if len(source_neighbor_list) == 0 or len(path) > max_hop:
            return False # no path found from v to u
        if u in source_neighbor_list:
            path.append(u)
            return path
        # Find a valid candidate for expansion
        v_prime = self.get_nearest_nbr_value_net(v, u)
        if(v_prime):
            return self.neural_nav(v_prime, u, path, max_hop, k)
        else:
            return False
 
    # Given source and target for a graph walk, finds a candidate node that
    # is closest using value_net calculation
    # If candidate list is given , considers only given candidate nodes
    # else considers all source neighbours as valid cadidates for selection
    def get_nearest_nbr_value_net(self, source, target, cand_list=[]):
        if(len(cand_list) == 0):
            nbrs = nx.all_neighbors(self.graph, source)
            source_neighbor_list = [n for n in nbrs]
            cand_list = source_neighbor_list
        try:
            shortest_v_u = nx.shortest_path_length(self.graph, source=source, target=target)
        except nx.exception.NetworkXNoPath:
            print("NO path exists between ", source, target)
            return False

        min_distance = float('inf')
        selected_node = -1
        for x in cand_list:
            total_distance = self.get_value_net(source, x, target, shortest_v_u)
            if(total_distance == False):
                continue
            elif(total_distance < min_distance):
                min_distance = total_distance
                selected_node = x
        if(selected_node == -1):
            print("Error: Could not find any node conecting source to target", source, target)
            return False
        return selected_node

    def get_value_net(self, source, x, target, shortest_v_u):
        try:
            shortest_x_u = nx.shortest_path_length(self.graph, source=x,
                                               target=target)
            shortest_v_x_u = shortest_x_u + 1 # length of shortest path from v to u through x
            f_reachability  = self._lambda * (float(shortest_v_x_u)/float(shortest_v_u))
            f_semantic  = (1.0 - self._lambda)  * self.emb_distance(x, target)
            total_distance = f_reachability + f_semantic
            return total_distance
        except nx.exception.NetworkXNoPath:
            print("NO path exists between ", source, target)
            return False




    #TODO FINISH IMPL
    def mcts_nav(self, v, u, max_nodes_to_visit):
        #Initialize search params
        nodes_to_visit = [v]
        visited_nodes = []
        # visit_count[x] = number of nodes visited in subtree[x] 
        visit_counts = dict.fromkeys(self.node_embeddings.keys(), 0)
        path_from_source_to_explored_candidate = [v]
        
        #Only explore some max nodes in graph
        while len(visited_nodes) <= max_nodes_to_visit:
            # If visited all possible candidates return False (no path found)
            if len(nodes_to_visit) == 0:
                return False
            # Select next visit candidates
            v_prime = self.sample_prob(v, nodes_to_visit, u, visit_counts)

            # Find its neighbors that have not been traversed yet
            v_prime_nbrs = [n for n in nx.all_neighbors(self.graph, v_prime)]
            v_prime_nbrs_unexpanded = [nbr for nbr in v_prime_nbrs if (nbr not in visited_nodes and nbr not in nodes_to_visit)]
            next_cand = self.get_nearest_nbr_value_net(v_prime, u, v_prime_nbrs_unexpanded) 
            nodes_to_visit.append(next_cand)
            last_half_path = self.neural_nav(next_cand, u)
            if(last_half_path):
                # TODO path_from_source_to_explored_candidate
                result_path = path_from_source_to_explored_candidate ++ last_half_path
                return result_path
            else:
                visit_count[next_cand] = sum([visit_count[nbr] for nbr in
                                              nx.all_neighbors(self.graph, next_cand)])
                visited_nodes.append(next_cand)
                nodes_to_visit.remove(next_cand)
                if self.explored_all_nbrs(v_prime, visited_nodes):
                    visited_nodes.append(v_prime)
                    nodes_to_visit.remove(v_prime)
        return False

    def explored_all_nbrs(self, v_prime, visited_nodes):
        v_prime_nbrs = nx.all_neighbors(self.graph, v_prime)
        for n in v_prime_nbrs:
            if n not in visited_nodes:
                return False
        return True




    def post_process_paths(self, paths):
        # TODO: Khushbu Implement path ranking  method
        # Sort paths by rank
        return paths[0]
                 
    def sample_prob(self, source, candidate_visit_nodes, target, visit_counts):
        if(len(candidate_visit_nodes)) == 1:
            return candidate_visit_nodes[0]
        candidate_visit_probs = [self.get_cand_prob(source, x, target, visit_counts[x]) for x in candidate_visit_nodes]
        selected_node_index = -1
        max_prob = -1.0
        for index, prob in enumerate(candidate_visit_probs):
            if(prob > max_prob):
                max_prob = prob
                selected_node_index = index
        return candidate_visit_nodes[selected_node_index]

    def get_cand_prob(self, source, x, target, visit_count_x):
        shortest_source_target = nx.shortest_path_length(self.graph, source=source,
                                                         target=target)
        f_v_x_u = self.get_value_net(source, x, target, shortest_source_target)
        prob_x = 1.0/(float(f_v_x_u) * float(1.0 + visit_count_x))
        return prob_x

    def post_process_paths(self, paths):
        print("Please implement path post porocessing")
        return

    def emb_distance(self, node1, node2):
        return distance.cosine(np.asarray(self.embeddings[node1], dtype='float32'),
                               np.asarray(self.embeddings[node2], dtype='float32'))

    def remove_direct_edge(self, source, dest):
        nbrs = nx.all_neighbors(self.graph, source)
        if dest in nbrs:
            self.graph.remove_edge(source, dest)

    def add_edge(self, source, dest):
        self.graph.add_edge(source, dest)


   
#Implements KDtree from scikitlearn , whihc uses    
class CustomKDTree:
    def __init__(self, node_embeddings, emb_size):
        self.integer_dict = dict()
        num_samples = len(node_embeddings)
        kdtree_array = np.empty(shape=(num_samples, emb_size), dtype=float)
        mapped_id = 0
        for node_id, emb in node_embeddings.items():
            kdtree_array[mapped_id] = emb 
            self.integer_dict[node_id] = mapped_id
            mapped_id += 1
        self.tree = sklearn.neighbors.KDTree(kdtree_array)

    def get_nearest_nbr(self, target_emb, k):
        mapped_id = self.integer_dict[node_id]
        dist, ind = self.tree.query(target_emb, k)
        print(dist, ind)
        actual_ids = [self.integer_dict[i] for i in ind]
        return actual_ids

