#!/usr/bin/python

import os
import sys
import numpy as np
import sklearn.metrics.distance as distance
import sklearn.neighbors.KDTree as KDTree
import load_data

class EmbNav:

    def __init__(self, graph, node_embeddings, num_nodes, emb_size, _lambda = 0.5):
        self.graph = graph
        self.embeddings = node_embeddings
        self.num_nodes = num_nodes
        self.emb_size = emb_size
        self._lambda = _lambda
        self.kdtree = CustomKDTree(node_embeddings, emb_size)
        self.visit_counts = dict()
        for k in node_embedding.keys():
            self.visit_counts[k] = 0


    # returns appended path if v->u path is explorable  or False otherwise
    def sim_nav(self, v, u, path):
        path.append(v)
        nbrs = nx.all_neighbours(self.graph, v)
        source_neighbor_list = [n for n in nbrs]
        if len(source_neighbor_list) == 0:
            return False # no path found from v to u
        if u in source_neighbor_list:
            path.append(u)
            return path
        v_prime = self.get_nearest_nbr_cosine(u, source_neighbor_list)
        return self.sim_nav(v_prime, u, path)
    
    # Finds nearest neighbour to target by embedding distanace
    # Embedding distance is calculated using cosine similarity
    def get_nearest_nbr_cosine(self, target, neighbor_list):
        #target_emb = np.asarray(self.embeddings[target]. dtype=float32)
        #neighbor_distances = [distance.cosine(np.asarray(self.embeddings[n],dtype='float32'),
        #                                      target_emb) for n in neighbor_list]
        neighbor_distances = [self.emb_distance(n, target) for n in
                              neighbor_list]
        neighbors_by_dist = [neighbor_list[i] for i in
                             np.argsort(neighbor_distances)]
        return neighbors_by_dist[0]


    # returns appended path if v->u path is explorable  or False otherwise
    def neural_nav(self, v, u, path):
        path.append(v)
        nbrs = nx.all_neighbours(self.graph, v)
        source_neighbor_list = [n for n in nbrs]
        if len(source_neighbor_list) == 0:
            return False # no path found from v to u
        if u in source_neighbor_list:
            path.append(u)
            return path
        v_prime = self.get_nearest_nbr_value_net(v, u)
        return self.neural_nav(v_prime, u, path)
 
    def get_nearest_nbr_value_net(self, source, target):
        nbrs = nx.all_neighbours(self.graph, source)
        source_neighbor_list = [n for n in nbrs]
        shortest_v_u = nx.shortest_path_length(self.graph, source=source, target=target)
        min_distance = float('inf')
        selected_node = -1
        for x in source_neighbor_list:
            total_distance = self.get_value_net(source, x, target, shortest_v_u)
            if(total_distance < min_distance):
                min_distance = total_distance
                selected_node = x
        if(selected_node == -1 or max_score == 0.0):
            print("Error: Could not find any node, with reasonable distance from
                  source to target", source, target)
            sys.exit(1)
        return selected_node

    def get_value_net(self, source, x, target, shortest_v_u):
        shortest_x_u = nx.shortest_path_length(self.graph, source=x,
                                               target=target)
        shortest_v_x_u = shortest_x_u + 1 # length of shortest path from v to u through x
        f_reachability  = self._lambda * (float(shortest_v_x_u)/float(shortest_v_u))
        f_semantic  = (1.0 - self._lambda)  * self.emb_distance(x, target)
        total_distance = f_reachability + f_semantic
        return total_distance



    def init_mctv_nav(self, max_node_visits):
        self.visited_nodes = list()
        self.max_node_visits = max_node_visits
        self.result_paths = []


    def mcts_nav(self, v, u, path, max_node_visits, post_process=False):
        #Initialize search params
        if len(path) == 0:
            self.init_mctv_nav(max_node_visits)

        self.visited_nodes.append(v)
        while len(self.visited_nodes) < self.max_node_visits:
            # if no more candidates left to explore, return false (no path
            # found)
            if len(self.visited_nodes) == 0
                return False
            v_prime = self.sample_prob(self.visited_nodes)
            v_prime_nbrs = [n for n in nx.all_neighbours(self.graph, v_prime)]
            v_prime_nbrs_unexpanded = [v for v in v_prime_nbrs if v not in self.visited_nodes]

            


        if post_process:
            return self.post_process_paths(self.result_paths)
        return self.result_paths

    def sample_prob(self, source, candidate_visit_nodes, target):
        candidate_visit_probs = [self.get_cand_prob(source, x, target) for x in candidate_visit_nodes]
        selected_node_index = -1
        max_prob = -1.0
        for index, prob in enumerate(candidate_visit_probs):
            if(prob > max_prob):
                max_prob = prob
                selected_node_index = index
        return candidate_visit_nodes[selected_node_index]

    def get_cand_prob(self, source, x, target):
        shortest_source_target = nx.shortest_path_length(self.graph, source=source,
                                                         target=target)
        f_v_x_u = self.get_value_net(source, x, target, shortest_source_target)
        visit_count_x = self.visit_counts[x]




        






    def post_process_paths(self, paths):
        print("Please implement path post porocessing")
        return

    def emb_distance(self, node1, node2):
        return distance.cosine(np.asarray(self.embeddings[node1], dtype='float32'),
                               np.asarray(self.embeddings[node2], dtype='float32'))


   
#Implements KDtree from scikitlearn , whihc uses    
class CustomKDTree:
    def __init__(self, node_embeddings, emb_size):
        self.integer_dict = dict()
        num_samples = len(node_embeddings)
        kdtree_array = np.array(num_samples, emb_size)
        mapped_id = 0
        for node_id, emb in node_embeddings.iteritems():
            kdtree_array[mapped_id] = emb 
            self.integer_dict[node_id] = mapped_id
            mapped_id += 1
        self.tree = KDTree(kdtree_array)

    def get_nearest_nbr(self, target_emb, k):
        mapped_id = self.integer_dict[node_id]
        dist, ind = self.tree.query(target_emb, k)
        print(dist, ind)
        actual_ids = [self.integer_dict[i] for i in ind]
        return actual_ids

