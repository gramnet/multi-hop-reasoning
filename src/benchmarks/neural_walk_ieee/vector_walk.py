#!/usr/bin/python
import tensorflow as tf
import numpy as np
#from tf.contrib.keras.callbacks import ModelCheckpoint
import sys, time
import keras
import h5py, configparser
import networkx as nx
from scipy.spatial import distance
#from mpi4py import MPI
from keras.models import load_model
from qa import generate_question_emb
from load_data import yago,wikispeedia,freebase,nell

use_mpi = False
app_config = configparser.ConfigParser()
if len(sys.argv) != 2:
  print("Provide path to config file")
  sys.exit(1)
app_config.read(sys.argv[1])
#if (app_config['training']['use_mpi'] == 'yes'):
#  use_mpi = True
#  comm = MPI.COMM_WORLD
#  rank = comm.Get_rank()
#  size = comm.Get_size()
use_mpi = False
rank = 0
size = 1

# Load data and evaluate model
def test_learn(app_config):
  seq_len = app_config['training'].getint('seq_len')
  embed_sz = app_config['training'].getint('embed_sz')
  batch_sz = app_config['training'].getint('batch_sz')
  dataset = app_config['files']['datatype']
  topk_nbrs_question = app_config['training'].getint('topk_nbrs')
  topk_nbrs_answers = app_config['test'].getint('topk_nbrs_test')
  training_path = app_config['files']['training_path']
  use_nbrs = app_config['test']['topk_selection']
  test_output =  app_config['test']['output_path'] + "_" + str(rank)
  path_comparison_output =  app_config['test']['path_comparison_output'] + "_" + str(rank)
  pattern_comparison_output =  app_config['test']['pattern_comparison_output'] + "_" + str(rank)
  if(dataset == "yago"):
    dataobj = yago()
  elif(dataset == "wikispeedia"):
    dataobj =wikispeedia()
  elif(dataset == "freebase"):
    dataobj = freebase()
  elif(dataset == "nell"):
    dataobj = nell()
  else:
    print("Unknown data format")
    sys.exit(1)
 
  # Load test paths and model
  #training_paths = dataobj.load_paths(app_config['files']['training_path'],use_mpi, size, rank)
  test_paths = dataobj.load_paths(app_config['files']['test_path'], use_mpi, size, rank)
  node_degrees = dataobj.load_degree(app_config['files']['node_degree_path'])
  node_embeddings = dataobj.load_node_label_embedding_map(app_config['files']['node_embedding_path'])
  node_categories = dataobj.load_node_label_categories_map(app_config['files']['node_categories_path'])
  graph = dataobj.load_graph(app_config['files']['graph_path'])
  #model = load_model(app_config['files']['model_path'])
  model = None
  test_sz = 1
  #test_sz = batch_sz * size
  #eval_questions(test_paths, node_embeddings, node_degrees, seq_len, test_sz,graph)
  eval_model(test_paths, model, graph, node_embeddings, node_degrees, node_categories, seq_len, topk_nbrs_question, topk_nbrs_answers, test_sz, test_output, path_comparison_output, pattern_comparison_output, use_nbrs)
 
  #eval_paths(training_paths,node_embeddings, node_degrees, dataset + ".training_paths.properties.tsv") 
  #eval_paths(test_paths,node_embeddings, node_degrees, dataset + ".test_paths.properties.tsv") 
  return


def eval_questions(test_paths, node_embeddings, node_degrees, seq_len, test_sz, graph):
  test_questions, _ = generate_test_data2(test_paths, seq_len, test_sz)
  print("##question_id, source, target, embeddng_dist, max_degree, shortest_path_len")
  for i, ques in enumerate(test_questions):
    # only consider max prediction for seq_len steps of sequence
      s = ques[0]
      t = ques[1]
      question_dist = distance.cosine(np.asarray(node_embeddings[s], dtype='float32'), np.asarray(node_embeddings[t],  dtype='float32'))
      question_max_deg = max(node_degrees[s], node_degrees[t])
      shortest_path_len = nx.shortest_path_length(graph, s, t)
      #print(i+1, question_dist, question_max_deg, shortest_path_len)
      print(i+1, s, t, question_dist, question_max_deg, shortest_path_len)
  return
       
      


def eval_paths(test_paths, node_embeddings, node_degrees, output_path):
  fout = open(output_path, "w+")
  for path in test_paths:
    if(len(path) > 1):
      coherence_path = get_coherence(path, node_embeddings)
      #autocorrelation_actual_path, avg_correlation = get_autocorrelation(actual_path, node_embeddings)
      coherence_path2 = get_coherence2(path, node_embeddings, 3)    
      max_degree_path = max([node_degrees[n] for n in path])
    else:
      coherence_path = "null"
      max_degree_path = "NA"
      coherence_path2 = "null"
    #fout.write(str(actual_path) + ", " + str(len(actual_path)) + ", " + str(coherence_actual_path) + ", " + str(coherence_actual_path2) + "," + str(autocorrelation_actual_path) + ", " + str(avg_correlation) + ", " + str(average_degree_actual_path) + "\n")
    str_path = ";".join(x for x in path)
    fout.write(str_path + "\t" + str(len(path)) + "\t" + str(coherence_path) + "\t" + str(coherence_path2) + "\t" + str(max_degree_path) + "\n")
  fout.close()

# Evaluate different models
def eval_model(all_test_paths, model, graph, node_embeddings, node_degrees, node_categories, seq_len, topk_nbrs_question, topk_nbrs_answers, test_sz, test_output, path_comparison_output, pattern_comparison_output, use_nbrs):
  #  generate test question, answer pairs , predict results
  #test_case_path_id, test_data, test_labels, test_paths = generate_test_data(all_test_paths, seq_len, test_sz)
  test_questions, test_actual_paths = generate_test_data2(all_test_paths, seq_len, test_sz)
  
  # For each question answer pair and corresponsding model output, get accuracy of path
  fout = open(test_output, "w")
  fout.write("Question, actual path, predicted labels\n")
  
  # single step model
  print("Predicting paths")
  predicted_paths = generate_paths(test_questions, model, graph, seq_len, topk_nbrs_question, topk_nbrs_answers, node_embeddings, node_degrees, fout, use_nbrs)
  print("Number of actual and predicted paths", len(test_questions), len(predicted_paths))
  for qp, actual_path, predicted in zip(test_questions, test_actual_paths, predicted_paths):
    fout.write(str(qp) + "\t"  + str(actual_path) + "\t" + str(predicted) + "\n")
  # Get accuracy and other metrics for actual and predicted paths
  compare_paths(test_actual_paths, predicted_paths, node_embeddings, node_degrees, path_comparison_output)

  # Get accuracy and other metrics for actual and predicted patterns 
  # execute only if all node_categories are known
  if(len(node_categories) == len(node_degrees)):
    path_patterns_actual = transform_path_to_patterns(test_actual_paths, node_categories)
    path_patterns_predicted = transform_multiple_paths_to_patterns(predicted_paths, node_categories)
    compare_path_patterns(path_patterns_actual, path_patterns_predicted, pattern_comparison_output)
  
  fout.close()
  return

# Generate predicted paths for all entity pairs
def generate_paths(questionpairs, model, graph, seq_len, topk_nbrs_questions, topk_nbrs_answers, node_embeddings, node_degrees, fout, use_nbrs):
  allPaths = []
  for qp in questionpairs:  
    pred_paths = generate_path(qp, model, graph, seq_len, topk_nbrs_questions, topk_nbrs_answers, node_embeddings, node_degrees, [qp[0]], use_nbrs)
    for p in pred_paths:
      p.insert(0, qp[0])
    #fout.write(str(qp) + "\t" + str(pred_paths) + "\n")
    #if(len(pred_paths) >= topk_nbrs_answers):
    #  pred_paths = get_topk_disparate_paths(pred_paths)
    allPaths.append(pred_paths)
  return allPaths


# Generate predicted paths for given pair of entities using model and graph properties
def generate_path(qp, model, graph, max_seq_len, topk_nbrs_questions, topk_nbrs_answers, node_embeddings, node_degrees, currpath, use_nbrs):
  curr_length = len(currpath)
  source = qp[0]
  target = qp[1]
  if(curr_length > max_seq_len or source == target):
    return []

  allPaths = []
   
  #print("generating paths for : ", str(qp), currpath)
  #question_emb = generate_question_emb([qp], graph, node_embeddings, topk_nbrs_questions, node_degrees)
  #question_emb =  np.asarray(question_emb)
  #print(np.shape(question_emb))
  #batch_result_embeddings = model.predict(question_emb, batch_size=1)
  #result_emb = batch_result_embeddings[0]
  
  # For simple walk, without model
  #if(use_nbrs == "target_coherence"):
  target_emb = np.asarray(node_embeddings[target], dtype=float)
  #print(target_emb)
  topk_nbrs = get_topk_neighbors(source, graph, node_embeddings, node_degrees, target_emb, topk_nbrs_answers)
  #else: #(model_coherence or TODO model_look_ahead)
  #  topk_nbrs = get_topk_neighbors(source, graph, node_embeddings, node_degrees, result_emb, topk_nbrs_answers)
  
  #print("Source, target, selected next hop nbrs", str(source), str(target), str(topk_nbrs))
  #print_embedding_distance_sourcenbrs_target(source, graph, node_embeddings,
  #                                           node_degrees, target_emb,
  #                                           topk_nbrs_answers)
  source_all_neighbors = nx.all_neighbors(graph, source)
  source_neighbor_list = [n for n in source_all_neighbors] 
  #if(target in topk_nbrs): 
  if(target in source_neighbor_list): 
    path = [target]
    allPaths.append(path)

  for n in topk_nbrs:
    if(n != target and n not in currpath):
      nbr_qp = [n, target]
      newpath =  list(currpath)
      newpath.append(n)
      nbr_paths = generate_path(nbr_qp, model, graph, max_seq_len, topk_nbrs_questions, topk_nbrs_answers, node_embeddings, node_degrees, newpath, use_nbrs)
      newpath = newpath[0:-1]
      #print("Found paths between", nbr_qp, nbr_paths)
      for p in nbr_paths:
        p.insert(0, n)
      allPaths.extend(nbr_paths)
  return allPaths

def print_embedding_distance_sourcenbrs_target(input_node, graph, node_embeddings, node_degrees, target_emb):
  
  neighbors = nx.all_neighbors(graph, input_node)
  neighbor_list = [n for n in neighbors]
  if(len(neighbor_list) > 10000):
    #print("Reached node with geater tahn 10K nbrs, sampling nbrs for :", input_node)
    neighbor_list = np.random.choice(neighbor_list, 10000)
  neighbor_distances = [distance.cosine(np.asarray(node_embeddings[n],
      dtype='float32'), target_emb) for n in neighbor_list]
  print("Neighbour cosine distances from target_emb:")
  for n, d in zip(neighbor_list, neighbor_distances):
      print(n,d)
  k = 10 if len(neighbor_list) > 10 else len(neighbor_list)
  neighbors_by_dist = [neighbor_list[i] for i in np.argpartition(neighbor_distances, k)]
  print("Top 10 neighbours close to target by embdding", neighbors_by_dist)
  return 

def get_topk_neighbors(input_node, graph, node_embeddings, node_degrees, pred_label_emb, k):
  neighbors = nx.all_neighbors(graph, input_node)
  neighbor_list = [n for n in neighbors]
  if(len(neighbor_list) <= k):
    return neighbor_list
  if(len(neighbor_list) > 10000):
    print("Reached node with geater tahn 10K nbrs, sampling nbrs for :", input_node)
    neighbor_list = np.random.choice(neighbor_list, 10000)
  neighbor_distances = [distance.cosine(np.asarray(node_embeddings[n],
      dtype='float32'), pred_label_emb) for n in neighbor_list]
  neighbors_by_dist = [neighbor_list[i] for i in np.argpartition(neighbor_distances, k)]
  return neighbors_by_dist[0:k] 

def generate_test_data2(graph_paths, seq_len, batch_sz):
  print('Converting paths to training data and labels ...')
  test_questions = []
  valid_test_paths = []
  # Only overriden for single step model
  for i, path in enumerate(graph_paths):
    # only consider max prediction for seq_len steps of sequence
    if len(path) >= seq_len:
      path = path[0:seq_len]
    # ignore test case if entities have a direct relation
    #if(len(path) >= 3):
    if(len(path) >= 2):
      test_questions.append([path[0], path[-1]])
      valid_test_paths.append(path)
  #num_test_seqs = batch_sz*int(len(test_questions)/batch_sz)
  #test_seqs = test_seqs[0:num_test_seqs]
  test_data = np.asarray(test_questions)
  print("Converting graph path to test seqs", len(graph_paths), len(test_questions))
  print(np.shape(test_data))
  #train_labels = np.asarray(labels)
  #print(np.shape(train_labels))
  return test_data, valid_test_paths

def generate_test_data(graph_paths, seq_len, batch_sz):
  print('Converting paths to training data and labels ...')
  test_seqs = []
  labels = []
  test_case_path_id = []
  valid_test_paths = []
  # Only overriden for single step model
  for i, path in enumerate(graph_paths):
    # only consider max prediction for seq_len steps of sequence
    if len(path) >= seq_len:
      path = path[0:seq_len]
    if(len(path) >= 3):
      test_seqs.append([path[0], path[-1]])
      labels.append(path[1])
      test_case_path_id.append(path)
      valid_test_paths.append(path)
      #for s_pos in range(0, seq_len-2):
      #  test_seqs.append([path[s_pos], path[-1]])
      #  labels.append(path[s_pos+1])
      #  test_case_path_id.append(path)
  num_test_seqs = batch_sz*int(len(test_seqs)/batch_sz)
  test_seqs = test_seqs[0:num_test_seqs]
  labels = labels[0:num_test_seqs]
  test_data = np.asarray(test_seqs)
  test_case_path_id = test_case_path_id[0:num_test_seqs]
  print("Converting graph path to test seqs", len(graph_paths), len(test_seqs))
  print(np.shape(test_data))
  #train_labels = np.asarray(labels)
  #print(np.shape(train_labels))
  return test_case_path_id, test_data, labels, valid_test_paths

def transform_path_to_patterns(paths, node_to_categories):
  results = []
  for path in paths:
    #print("Transforming: path", path)
    categorical_path = []
    for n in path:
      if n in node_to_categories:
        x = node_to_categories[n] 
      else: 
        x = "NONE"
      categorical_path.append(x)
    results.append(categorical_path)
  return results

def transform_multiple_paths_to_patterns(paths, node_to_categories):
  allPatterns = []
  for entity_pair_paths in paths:
    entity_pair_patterns = []
    for path in entity_pair_paths:
      #print("Transforming: path", path)
      entity_pair_pattern = []
      for n in path:
        if n in node_to_categories:
          x = node_to_categories[n]
        else:
          x = "NONE"
        entity_pair_pattern.append(x)
      entity_pair_patterns.append(entity_pair_pattern)
    allPatterns.append(entity_pair_patterns)
  return allPatterns

#TODO
#def get_topk_disparate_paths(pred_paths, k, node_embeddings, node_degrees):
#  samples = []
#  for full_path in pred_paths:
#    predicted_nodes = full_path[1:-1] 
#    path_average_degree = np.average([node_degrees[n] for n in predicted_nodes])
#    path_coherence = get_coherence(predicted_nodes, node_embeddings)
#    path_len = len(predicted_nodes)
#    samples.append([path_average_degree, path_coherence, path_len])
# # sample here from each class
#  results = pred_paths
#  return  results[0:k]
    


 #Note: len(predicted path) and len(actual path) could be different
def compare_paths(actual_paths, predicted_paths, node_embeddings, node_degrees, path_comparison_output):
  fout = open(path_comparison_output, "w")
  for actual_path, topk_predicted_path in zip(actual_paths, predicted_paths):
    coherence_actual_path = get_coherence(actual_path, node_embeddings)
    #autocorrelation_actual_path, average_correlation_actual = get_autocorrelation(actual_path, node_embeddings)
    coherence_actual_path2 = get_coherence2(actual_path, node_embeddings, 3)    
    max_degree_actual_path = max([node_degrees[n] for n in actual_path])
    fout.write(str(actual_path) + "\t" + str(len(actual_path)) + "\t" + str(coherence_actual_path) + "\t" + str(coherence_actual_path2) + "\t"  + str(max_degree_actual_path) + "\t")
    for predicted_path in topk_predicted_path:
      num_same_nodes = 0
      for actual_node in actual_path:
        if(actual_node in predicted_path):
          num_same_nodes += 1
      percentage_same_nodes = (num_same_nodes*2)/(len(actual_path) + len(predicted_path))
      if(len(predicted_path) > 1):
        coherence_predicted_path = get_coherence(predicted_path, node_embeddings)
        max_degree_predicted_path = max([node_degrees[n] for n in predicted_path])  
        coherence_predicted_path2 = get_coherence2(predicted_path, node_embeddings, 3)
        #autocorrelation_predicted_path, average_correlation_predicted = get_autocorrelation(predicted_path, node_embeddings)
      else:
        coherence_predicted_path = "null"
        max_degree_predicted_path = "NA"
        coherence_predicted_path2 = "null"
      fout.write(str(predicted_path) + "\t" + str(len(predicted_path)) + "\t" + str(coherence_predicted_path) + "\t" + str(coherence_predicted_path2) + "\t" + str(max_degree_predicted_path) + "\t" + str(percentage_same_nodes) + "\t")
    fout.write("\n")
  fout.close()
  return 

def compare_path_patterns(actual_path_patterns, predicted_path_patterns, path_pattern_output):
  fout = open(path_pattern_output, "w")
  for actual_pattern, qp_predicted_pattern in zip(actual_path_patterns, predicted_path_patterns):
    fout.write(str(actual_pattern) + "\t")
    for predicted_pattern in qp_predicted_pattern:
      num_same_nodes = 0
      for actual_pattern_node in actual_pattern:
        if(actual_pattern_node in predicted_pattern):
          num_same_nodes += 1
      fout.write(str(predicted_pattern) + "\t" + str((num_same_nodes*2)/(len(actual_pattern) + len(predicted_pattern))) + "\t")
    fout.write("\n")
  fout.close()
  return

def get_coherence(path, node_embeddings):
  path_embeddings = [node_embeddings[n] for n in path]
  total_dist = 0.0
  for i in range(0, len(path)-1):
    tmp = distance.cosine(np.asarray(path_embeddings[i],
      dtype='float32'), np.asarray(path_embeddings[i+1],
      dtype='float32'))
    #print("Pair wise distance", path[i], path[i+1], tmp)
    total_dist = total_dist + tmp
  return total_dist/np.float(len(path)-1)
  

# Returns sigma_square 
# \operatorname {Var} (X)=\operatorname {Expectation} \left[(X-\mu )^{2}\right].
def get_path_variance(path_embeddings):
  average_path_embedding = np.average(np.asarray(path_embeddings, dtype='float32'), axis=0)
  total_distance = 0.0
  for i in range(0, len(path_embeddings)):
    d = distance.cosine(np.asarray(path_embeddings[i], dtype='float32'), np.asarray(average_path_embedding,  dtype='float32'))
    total_distance += (d*d)
  return total_distance/(len(path_embeddings)-1)


#Implements R(\tau )={\frac {\operatorname {E} [(X_{t}-\mu )(X_{t+\tau }-\mu )]}{\sigma ^{2}}},\,
def get_autocorrelation(path, node_embeddings):
  path_len = len(path)
  path_embeddings = [node_embeddings[n] for n in path]
  average_path_embedding = np.average(np.asarray(path_embeddings,dtype='float32'), axis=0)
  path_variance = get_path_variance(path_embeddings)
  rho_per_lag = []
  for lag in range(1, len(path)):
    seq_rho = 0.0
    for start_index in range(0, path_len-lag):
      x_s_distance_to_average = distance.cosine(np.asarray(path_embeddings[start_index], dtype='float32'), np.asarray(average_path_embedding,  dtype='float32'))
      x_t_distance_to_average = distance.cosine(np.asarray(path_embeddings[start_index+lag], dtype='float32'), np.asarray(average_path_embedding,  dtype='float32'))
      seq_rho += (x_s_distance_to_average*x_t_distance_to_average)/path_variance
    seq_rho = seq_rho/(path_len-lag)
    rho_per_lag.append(seq_rho)
  return (rho_per_lag, np.average(rho_per_lag))

# Implements metro maps coherence
# Identify top k concepts in the path
# Look at divergence of the path in those concepts only
# consider the influence of each concept
# returns the minimal coherent link for the maximal coherent topic
def get_coherence2(path, node_embeddings, num_active_concepts):
  path_len = len(path)
  path_embeddings = [node_embeddings[n] for n in path]
  average_path_embedding = np.average(np.asarray(path_embeddings, dtype='float32'), axis=0)
  # get active concept
  sorted_concept_ids = np.argsort(average_path_embedding)
  selected_concept_ids = sorted_concept_ids[-num_active_concepts:]
  #print("average embedding and active concepts", average_path_embedding, selected_concept_ids)
  max_coherence_concept = 0.0
  # get_path as a sequence of node_embeddings for these active concepts
  path_embeddings_selected_concepts = []
  for i in range(0, path_len):
    node_embedding = path_embeddings[i]
    node_embedding_selected_concept = [node_embedding[concept_id] for concept_id in selected_concept_ids]
    path_embeddings_selected_concepts.append(node_embedding_selected_concept)
  average_path_embeddings_selected_concepts = [average_path_embedding[i] for i in selected_concept_ids]

  #Calculate pair wise divergence of sequence per concept
  min_distance = []
  for concept_id in range(0, num_active_concepts):
    max_dist = -1.0
    for pos in range(0, path_len-1):
      d = abs(float(path_embeddings_selected_concepts[pos][concept_id]) - float(path_embeddings_selected_concepts[pos+1][concept_id]))
      dist = d*d
      if(dist > max_dist):
         max_dist = dist
    min_distance.append(max_dist)
    #min_distance.append(max_dist*average_path_embeddings_selected_concepts[concept_id])
  return(min(min_distance))
    
if __name__ == "__main__":
  test_learn(app_config)


