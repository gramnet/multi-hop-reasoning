#!/usr/bin/python

#import tensorflow as tf
import numpy as np
import sys, time
import keras
import h5py, configparser
import networkx as nx
import json
from load_data import yago,wikispeedia,freebase,nell,metaqa
from simnav import EmbNav

def main(argv):
    app_config = configparser.ConfigParser()
    if len(argv) != 3:
        print("Provide <path to config file> <select navigation type sim/neural/mcts>")
        sys.exit(1)
    app_config.read(argv[1])
    nav_type = argv[2]
    use_mpi = False
    rank = 0
    size = 1
    dataset = app_config['files']['datatype']
    emb_size = app_config['training'].getint('embed_sz')
    if(dataset == "yago"):
        dataobj = yago()
    elif(dataset == "wikispeedia"):
        dataobj =wikispeedia()
    elif(dataset == "freebase"):
        dataobj = freebase()
    elif(dataset == "nell"):
        dataobj = nell()
    elif(dataset == "metaqa"):
         dataobj = metaqa()
    else:
        print("Unknown data format")
        sys.exit(1)
    graph = dataobj.load_graph(app_config['files']['graph_path'])
    node_embeddings = dataobj.load_node_label_embedding_map(app_config['files']['node_embedding_path'])
    _lambda = 0.5
    topk_nbrs = app_config['test']['topk_nbrs_test']
    test_paths = dataobj.load_paths(app_config['files']['test_path'], use_mpi,size, rank)
    emb_nav = EmbNav(graph, node_embeddings, emb_size, _lambda)
    num_success = 0
    #Test case for no path available
    results = dict()
    for path in test_paths:
        source = path[0]
        dest = path[-1]
        emb_nav.remove_direct_edge(source, dest)
        if nav_type == "sim":
            predicted_paths = emb_nav.sim_nav(source, dest, [], max_hop=5,
                                              k=topk_nbrs)
        elif nav_type == "neural":
            predicted_paths = emb_nav.neural_nav(source, dest, [], max_hop=5, k=topk_nbrs)
        elif nav_type == "mcts":
            predicted_paths = emb_nav.mcts_nav(source, dest,
                                               max_nodes_to_visit=0.01*graph.num_nodes(), k=topk_nbrs)
        else:
            print("Unsuuported navigation type")
            return

        if(predicted_paths):
            num_success += 1
            results[source + "," + dest] = [predicted_paths]
            print("Found path ", source, dest, str(predicted_paths))
        else:
            print("No Path found", source, dest)
        emb_nav.add_edge(source, dest)
    print("Total success = ", num_success, len(test_paths))
    outfile = dataset + "." + nav_type + ".dict.txt"
    print("Saving results to ", outfile)
    with open(outfile, 'w') as f:
        json.dump(results, f, indent=4)

    return

if __name__ == "__main__":
    main(sys.argv)





