#!/usr/bin/python
import os, sys, io
import json

def getVertexDict(filename):
    my_dict = {}
    print("trying to get vertex dict: ", filename)
    fdict = io.open(filename, encoding ='utf8')
    for line in fdict:
        if(line):
            arr = line.strip().split("\t")
            if(len(arr) == 2):
                my_dict[int(arr[1].strip())] = arr[0].strip()
    print("Number of nodes in dict =", len(my_dict)) 
    fdict.close()
    return my_dict
    
def convertNodeIdToLabel(vertex_dict,embfile, outfile):
    print("trying to read node2vec embedding : ", embfile)
    fin = open(embfile, "r")
    fout=io.open(outfile, mode="w+", encoding = 'utf8')
    #ignore header
    header = fin.readline()
    fout.write(unicode(header))
    for line in fin:
        if(len(line) != 0):
            if(line[0] != '#' and line[0] != '@'):
                arr = line.strip().split(" ")
                nodeid = int(arr[0].strip())
                emb = arr[1:]
                embstr = "\t".join(x for x in emb)
                if(nodeid in vertex_dict):
                    node_label = vertex_dict.get(nodeid)
                    fout.write(node_label + "\t" + embstr + "\n")
                else:
                    print("Found a node id not in dict =", nodeid)
    fin.close()
    fout.close()

if __name__ == "__main__":
    if(len(sys.argv) !=3):
        print("Usage <vertex_dict_file> <node2vec_emb_file>")
        sys.exit(1)

    vertex_dict_file = sys.argv[1]
    node2vec_emb_file = sys.argv[2]
    outfile = node2vec_emb_file + ".labeled.txt"

    if vertex_dict_file.endswith(".json"):
        label_id_dict = json.load(io.open(vertex_dict_file, encoding='utf8'))
        vertex_dict =  {v: k for k, v in label_id_dict.iteritems()}
    else:
        vertex_dict = getVertexDict(vertex_dict_file)
    convertNodeIdToLabel(vertex_dict, node2vec_emb_file, outfile)
