#!/usr/bin/python

import os, sys

# For each question: find the best /NONE answer that minerva generated
def parse_minerva_output_paths(output_dir):
  question_answer_dict = {}
  for filename in os.listdir(output_dir):
    if(filename.startswith("paths_")):
      filepath = output_dir + "/" + filename
      print("Trying to read from ", filepath)
      f = open(filepath, "r")
      lines = f.readlines()
      for lineid in range(0, len(lines)):
        if(lineid == 0):
             question, answer, score = process_next_question(lines[lineid:])
        if(lines[lineid].startswith("########") and lineid +5 < len(lines)):
            #print("Found next question at ", lineid+1)
            question, answer, score  = process_next_question(lines[lineid+1:])
        question_answer_dict[(question[0], question[1])] = (answer, score)
      f.close()
  return question_answer_dict

def process_next_question(lines):
  question = lines[0].strip().split("\t")
  best_answer = []
  score = -1.0
  if(len(question) != 2):
    print("REading format error, expecting question separated by tab, exiting ..", line)
    sys.exit(1)
  
  reward = lines[1].strip()
  #print("Question, reward", question, reward)
  if(reward == "Reward:0"):
    #print("MInerva did not find a path for :", question)
    return (question, best_answer, score)
  elif(reward == "Reward:1"):
      for lineid in range(2, len(lines), 5):
        if(lines[lineid].startswith("######")):
          #print("FInished question, found no valid path", question)
          return(question, [], -1.0)
        else:
          answer = lines[lineid].strip().split("\t")
          path_pattern = lines[lineid+1].strip()
          is_sucessful_path = lines[lineid+2].strip()
          #blank_line = lines[lineid+4].strip()
          if(int(is_sucessful_path.strip()) == 1):# and not is_cyclic(answer)):
            if(is_cyclic(answer) == False):
              minerva_score = float(lines[lineid+3].strip())
              return(question, answer, minerva_score)
            else:
              #print("FOund a cyclic answer", answer)
              continue
  # if all paths, minerva found were cyclic paths, return empty
  return (question, best_answer, score)


def is_cyclic(answer):
  node_dict = {}
  for node in answer:
    if(node in node_dict):
      return True
    else:
      node_dict[node] = 1
  return False


if __name__ == "__main__": 
  if(len(sys.argv) != 3):
    print ("Usage <minerva_outdir> <output_file_name>")
    sys.exit(1)
  question_answer_score_dict = parse_minerva_output_paths(sys.argv[1])
  num_success = 0
  num_not_sucess = 0
  #outfile = sys.argv[1] + "/processed_minerva_path_output.tsv"
  outfile = sys.argv[2]
  outfile_valid_path_only = outfile + ".valid_paths.txt"
  print("Writing complete output to ", outfile)
  print("Writing valid paths only  to ", outfile_valid_path_only)
  fout = open(outfile, "w")
  fout2 = open(outfile_valid_path_only, "w")
  for k,v in question_answer_score_dict.iteritems():
      path = ";".join(x for x in v[0])
      fout.write(str(k) + "\t" + str(v[0]) + "\t" + str(v[1]) + "\n")
      if(len(v[0]) > 0):
          path = ";".join(x for x in v[0])
          fout2.write(path + "\n")
          num_success += 1
      else:
          num_not_sucess += 1
  print("Number of sucess out of ", num_success,
        len(question_answer_score_dict))
  fout.close()


    
