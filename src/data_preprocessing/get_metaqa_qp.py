#!/usr/bin/python
import io
import sys
import json
import networkx as nx

class MetaQAParser:

    def __init__(self, init_dir, question_dir):
        self.dir = init_dir
        self.node_types = dict()
        self.kb = self.load_kb(init_dir + "/kb.txt")
        print("Loaded KB (#nodes, #edges) = ", self.kb.number_of_nodes(), self.kb.number_of_edges())
        qdir = init_dir + "/" + question_dir + "/" 
        str_node_type_dict = dict()
        for k,v in self.node_types.iteritems():
            str_node_type_dict[k] = list(v)
        #self.json_dump_unicode(str_node_type_dict, init_dir + "/node_types.json")
        self.node_types = self.load_node_types(init_dir + "/node_types.json")
        hop = 2 if question_dir == "2-hop-vanilla" else 3
        print("Running question_gen on ", qdir)
        
        outfile = qdir + "training_qp_paths.json"
        self.train_qp = self.generate_entity_pairs(qdir + "qa_train.txt")
        self.train_patterns = self.get_answer_patterns(qdir + "qa_train_qtype.txt")
        self.train_qp_paths_all = self.generate_paths_by_pattern(self.train_qp,
                                                                 self.train_patterns, outfile, hop)
        
        outfile = qdir + "dev_qp_paths.json"
        self.valid_qp = self.generate_entity_pairs(qdir + "qa_dev.txt")
        self.valid_patterns = self.get_answer_patterns(qdir + "qa_dev_qtype.txt")
        self.train_qp_paths_all = self.generate_paths_by_pattern(self.valid_qp,
                                                                 self.valid_patterns, outfile, hop)
        
        outfile = qdir + "test_qp_paths.json"
        self.test_qp = self.generate_entity_pairs(qdir +"qa_test.txt")
        self.test_patterns = self.get_answer_patterns(qdir + "qa_test_qtype.txt")
        self.train_qp_paths_all = self.generate_paths_by_pattern(self.test_qp,
                                                                 self.test_patterns, outfile, hop)

    def load_kb(self, filename, sep='|'):
        G = nx.MultiGraph()
        with io.open(filename, "r", encoding='utf8') as f:
            for line in f:
                arr = line.strip().split(sep)
                self.add_node_types(arr[0], arr[1], arr[2])
                G.add_edge(arr[0], arr[2], label=arr[1])
        return G

    #Infers node type of source and target from edge type for MetaQA edges
    def add_node_types(self, source, edge_label, target):
        if(source not in self.node_types):
            self.node_types[source] = set()
        if(target not in self.node_types):
            self.node_types[target] = set()


        if edge_label == "directed_by":
            self.node_types[source].add("movie")
            self.node_types[target].add("director")
        elif edge_label == "has_genre":
            self.node_types[source].add("movie")
            self.node_types[target].add("genre")
        elif edge_label == "has_imdb_rating":
            self.node_types[source].add("movie")
            self.node_types[target].add("rating")
        elif edge_label == "has_imdb_votes":
            self.node_types[source].add("movie")
            self.node_types[target].add("votes")
        elif edge_label == "has_tags":
            self.node_types[source].add("movie")
            self.node_types[target].add("tag")
        elif edge_label == "in_language":
            self.node_types[source].add("movie")
            self.node_types[target].add("language")
        elif edge_label == "release_year":
            self.node_types[source].add("movie")
            self.node_types[target].add("year")
        elif edge_label == "starred_actors":
            self.node_types[source].add("movie")
            self.node_types[target].add("actor")
        elif edge_label == "written_by":
            self.node_types[source].add("movie")
            self.node_types[target].add("writer")
        else:
            print("Could not find this as valid edge", source,
                  edge_label, target)
            return
    
    def load_node_types(self, filename):
        return json.load(io.open(filename, 'r', encoding='utf8'))

    # Reads Metaqa natural language question and answer file in following format
    # "natural_languge_question1 <tab> answer_entity1|answer_entity2|..."
    # generates double list containing question entity pairs (e1,e2)
    # [
    # [(question_entity_line1, answer_entity1), (question_entity_line1, answer_entity2)..,
    # [(question_entity_line2, answer_entity..], ...
    # ]
    # Note double list allows to map question per line "qa_*" to a given answer pattern in "qa_*_qtype"
    def generate_entity_pairs(self, question_file):
        qa_pairs = list()
        with io.open(question_file, "r", encoding='utf8') as f:
            for line in f:
                qa = line.strip().split("\t")
                question_entity = self.get_bracketed_entity(qa[0])
                answer_entities = qa[1].split("|")
                current_qa_pairs = [(question_entity, answer_entity) for answer_entity in answer_entities]
                qa_pairs.append(current_qa_pairs)
        return qa_pairs
    
    def get_bracketed_entity(self, string_question):
        start = string_question.find("[")
        end = string_question.find("]")
        return string_question[start+1:end]

    def get_answer_patterns(self, answer_pattern_file):
        answer_patterns = []
        with open(answer_pattern_file, "r") as f:
            for line in f:
                arr = line.strip().replace("_to", "").split("_")
                answer_patterns.append(arr)
        return answer_patterns

    def generate_paths_by_pattern(self, qpairs, answer_patterns, outfile, hop):
        print("Processing #question pairs for hop", len(qpairs), hop)
        qa_dict = dict()
        f = io.open(outfile + ".question_triples", "w", encoding="utf8")
        for question_pairs, answer_pattern in zip(qpairs, answer_patterns):
            for qp in question_pairs:
                #tmp = "_".join(answer_pattern)
                #f.write(qp[0] + "\t" + tmp + "\t" + qp[1] + "\n")
                valid_paths = self.generate_path(qp, answer_pattern, hop)
                question_key = qp[0] + "," + qp[1]
                qa_dict[question_key] = valid_paths
                print(valid_paths)
        self.json_dump_unicode(qa_dict, outfile)
        return
    
    def get_nbrs_of_type(self, source, nbr_type):
        all_nbrs = list(nx.all_neighbors(self.kb, source))
        valid_nbrs = [x for x in all_nbrs if nbr_type in self.node_types[x]]
        return valid_nbrs

    # finds all paths of 2-hop or 3-hop for question-pair,  that match the pattern 
    # returns empty list is none exists
    def generate_path(self, qp, pattern, hop):
        source = qp[0]
        target = qp[1]
        assert(pattern[0] in self.node_types[source])
        assert(pattern[-1] in self.node_types[target])
        if(hop == 2):
            assert(len(pattern) == 3)
            common_nbrs = set(self.get_nbrs_of_type(source, pattern[1])) & \
                    set(self.get_nbrs_of_type(target, pattern[1]))
            if len(common_nbrs) == 0:
                print("source, target have no common nbrs : ", source, target)
            paths = [[source, nbr, target] for nbr in common_nbrs]
            return paths
        elif(hop == 3):
            assert(len(pattern) == 4)
            src_matching_nbrs = set(self.get_nbrs_of_type(source, pattern[1]))
            dest_matching_nbrs = set(self.get_nbrs_of_type(target, pattern[2]))
            paths = []
            for src_nbr in src_matching_nbrs:
                src_nbr_hop2_cands = [x for x in nx.all_neighbors(self.kb, src_nbr)]
                valid_cands_hop2 = set(src_nbr_hop2_cands).intersection(dest_matching_nbrs)
                for cand in valid_cands_hop2:
                    path = [source, src_nbr, cand, target]
                    paths.append(path)
            return paths

    def json_dump_unicode(self, mydict, outfile):
        with io.open(outfile, "w", encoding='utf8') as json_file:
            data = json.dumps(mydict, ensure_ascii=False, indent=4)
            json_file.write(unicode(data))


    def run_path_gen_test(self):
        test_pair = ("Maureen Medved", "Drama")
        test_pattern = ["writer", "movie", "genre"]
        hop = 2
        paths = self.generate_path(test_pair, test_pattern, hop)
        print(paths)
        assert(str(paths[0]) == str(["Maureen Medved", u"The Tracey Fragments", "Drama"]))

    def run_test_node_type(self):
        assert("movie" in self.node_types[u"The Tracey Fragments"])
        assert("movie" in self.node_types[u'Rachel Getting Married'])
        assert("director" in self.node_types[u'William Dieterle'])


if __name__ == "__main__":
    if(len(sys.argv) != 3):
        print("Usage <path_metaqa_dir> <question_dir>")
        sys.exit(1)
    MetaQAParser(sys.argv[1], sys.argv[2])

