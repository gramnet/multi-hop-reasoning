#!/usr/bin/python
import numpy as np
import io, sys
import configparser
import networkx as nx
# reads NELL format data as provided by UCSB at this link:
# http://cs.ucsb.edu/~xwhan/datasets/NELL-995.zip
# creates a sepaerate node degree file and a labeled embedding file for use in
# path feature metrics later


app_config = configparser.ConfigParser()
dataname = "nell"
config_file= "../../config/" +  dataname  + "/" + dataname + ".cfg"
print("Reading config from..", config_file) 
app_config.read(config_file)


def get_vertex_dict(filename):
    my_dict = {}
    print("trying to get vertex dict: ", filename)
    fdict = io.open(filename, encoding ='utf8')
    for line in fdict:
        if(line):
            arr = line.strip().split("\t")
            if(len(arr) == 2):
                my_dict[int(arr[1].strip())] = arr[0].strip()
    print("Number of nodes in dict =", len(my_dict))
    fdict.close()
    return my_dict

def get_graph(graphfile):
  G = nx.Graph()
  f = open(graphfile, "r")
  for line in f:
    if(len(line) > 0  and not line.startswith("#")):
      arr = line.strip().split("\t")
      if(len(arr) == 3):
        G.add_edge(arr[0], arr[2], label=arr[1])
      elif(len(arr) == 2):
        G.add_edge(arr[0], arr[1])
      else:
        print("find a line in expected format", line)
  print(G.number_of_nodes(), G.number_of_edges())
  f.close()
  return G

def create_labeled_embedding_file(init_embedding_file, node_dict):
    f = open(init_embedding_file)
    fout = open(init_embedding_file + ".labeled", "w")
    id = 0
    for line in f:
        node_label = node_dict[id] 
        outline = node_label + "\t" + line
        fout.write(outline)
        id += 1
    f.close()
    fout.close()

def create_labeled_embedding_node2vec(init_embedding_file, node_dict):
    f = open(init_embedding_file)
    fout = open(init_embedding_file + ".labeled", "w")
    header = f.readline()
    for line in f:
        arr = line.strip().split()
        id = int(arr[0])
        node_label = node_dict[id] 
        emb_str = "\t".join(x for x in arr[1:])
        outline = node_label + "\t" + emb_str + "\n"
        fout.write(outline)
    f.close()
    fout.close()

def create_node_category_file(node_dict, outfile):
  node_category_dict = {}
  for nodeid, node_label in node_dict.iteritems():
    if(node_label.startswith("concept_")):
      node_category_start = node_label[8:]
      node_category_end = node_category_start.find("_")
      node_category = node_label[:node_category_end]
      node_category_dict[node_label] = node_category
    else:
      print("No category found for, adding NONE ", node_label)
      node_category_dict[node_label] = "NONE"

  print("Size of node label dictionary", len(node_category_dict))
  with open(outfile, "w") as f:
      for k,v in node_category_dict.iteritems():
          f.write(str(k)  + "\t" + str(v) + "\n")

  return node_category_dict


# create node_degree, node_embedding, node_category file per vertex in the
# graph
if __name__ == "__main__": 
  datahome = app_config['files']['datahome']
  #graph = get_graph(app_config['files']['graph_path'])
  # create a node degree file for later analysis 
  #node_deg_iter = graph.degree_iter()
  #node_degree_file = open(datahome + "/node_degrees.txt" , "w")
  #for k,v in node_deg_iter:
  #  node_degree_file.write(k + "\t" + str(v) + "\n")
  #  create an embdding file by labels 
  node_dict = get_vertex_dict(datahome + "/entity2id.txt")
  #create_node_category_file(node_dict, datahome+"/node_categories.txt")
  #init_embedding_file = datahome + "/entity2vec.bern"
  init_embedding_file = datahome + "/nell.emb.p1.q1.w20.l10.e100"
  #create_labeled_embedding_file(init_embedding_file, node_dict)
  create_labeled_embedding_node2vec(init_embedding_file, node_dict)
